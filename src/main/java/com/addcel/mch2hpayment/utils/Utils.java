package com.addcel.mch2hpayment.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.utils.AddcelCrypto;

public class Utils {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("ddhhmmssSSS");
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
		
	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_Antad_header_600.PNG";
			
	public Utils() {
		
	}
	
	public static String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			digest = bigIntDgst.toString(16);
			digest = String.format("%040x", bigIntDgst);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	public static String getFecha(String formato){
        String dateString = null;
        SimpleDateFormat format = new SimpleDateFormat(formato);
        try {
        	Date now = new Date();
        	dateString = format.format(now);
        }
        catch(Exception pe) {
            System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
        }
		return dateString;
	}
	
	public static String encryptJson(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
	}
	
	public static String decryptJson(String json){
		return AddcelCrypto.decryptSensitive(json);
	}

	public static String encryptHard(String json){
		return AddcelCrypto.encryptHard(json);
	}
	
	public static String formatoMontoPayworks(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+ "." + centavos;
        }else{
            varTotal=monto.concat(".00");
        } 
		return varTotal;		
	}
	
	public static String cambioAcento(String cadena){
		try{
			if(cadena != null){
				cadena = cadena.replaceAll("Ã¡", "á");
				cadena = cadena.replaceAll("Ã³", "ó");
			}
		}catch(Exception e){
			LOGGER.error("Error al cambiar acentos: " + e.getMessage());
		}
		return cadena;
	}
	
}