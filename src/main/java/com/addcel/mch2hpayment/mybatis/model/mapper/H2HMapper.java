package com.addcel.mch2hpayment.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.mch2hpayment.client.model.AccountMC;
import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.TransaccionBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.User;

public interface H2HMapper {

	public String H2HBanorte_Business(@Param(value = "idUsuario") long idUsuario , @Param(value = "idBitacora") long idBitacora, @Param(value = "transactionNumber") long transactionNumber, @Param(value = "alias") String alias, @Param(value = "proceso") int proceso , @Param(value = "idAccount") long idAccount ,@Param(value = "telefono") String telefono,@Param(value = "correo") String correo,@Param(value = "nidioma") String nidioma,@Param(value = "idApp") int idApp);
	public int isBinValido(@Param(value = "bin")String bin);
	public long getMaxIdBitacora();
	public List<BankCodes> getBankcodes(); 
	
	//payworks
	public Card getCard(@Param(value = "idUsuario") long idUsuario, @Param(value = "idTarjeta") long idTarjeta, @Param(value = "idApp") int idApp);
	public String RulesH2HBanorte(@Param(value = "idUser") long idUser,@Param(value = "accountId") long accountId, @Param(value = "cardId") long cardId,@Param(value = "amount_transfer") Double amount_transfer,@Param(value = "bin") String bin,@Param(value = "idioma") String idioma, @Param(value = "idApp") int idApp);
	public User getUserData(@Param(value = "idUsuario") long idUsuario, @Param(value = "idApp") long idApp);
	public AccountRecord getAccount(@Param(value = "idaccount") long idaccount);
	public BankCodes getBankcodesByClave(@Param(value = "clave_banco") String clave_banco);
	public Proveedor getProveedor(String proveedor);
	public AfiliacionVO buscaAfiliacion(@Param(value = "banco")String banco);
	
	public void addBitacora(TBitacoraVO bitacora);
	public void  updateTbitacora(@Param(value = "idBitacora") long idBitacora,@Param(value = "status") int status, @Param(value = "aut") String aut);
	public void insertTbitacoraMCBanorte(TBitacoraBanorte bitacora);
	public TBitacoraBanorte getBitacoraBanorte(@Param(value = "idBitacora") long idBitacora);
	public void updateBitacoraBanorteOBJ(TBitacoraBanorte bitacora);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	public String getParameter(@Param(value = "parameter") String parameter);
	public void insertaTransactionBanorte(TransaccionBanorte bitacora);
	public TransaccionBanorte getTransactionBanorte(@Param(value = "id_bitacora") long id_bitacora);
	public String getToken();
	public int difFechaMin(String token);
	public List<Bank> getCatalogoBancos();
	public void insertAccount(AccountMC account);
	public Bank getCatalogoBancosByClave(@Param(value = "clave") String clave);
	public String isAppActive(@Param(value = "idApp") Integer idApp);
}
