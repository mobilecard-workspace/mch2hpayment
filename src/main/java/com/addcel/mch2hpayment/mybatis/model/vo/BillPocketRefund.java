package com.addcel.mch2hpayment.mybatis.model.vo;

public class BillPocketRefund {

	private long idTransaccion;

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
}
