package com.addcel.mch2hpayment.mybatis.model.vo;

public class Afiliacion {
	
	private long ID; 
	private String AFILIACION;
	private String USUARIO;
	private String CLAVE_USUARIO;
	private String MERCHANT_ID;
	private String ID_TERMINAL;
	private String ADDRESS;
	private String COMERCIO;
	private int ACTIVO;
	private String BANCO;
	private String CURRENCY;
	private String STORE;
	private String FORWARD_PATH;
	
	public Afiliacion() {
		// TODO Auto-generated constructor stub
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getAFILIACION() {
		return AFILIACION;
	}

	public void setAFILIACION(String aFILIACION) {
		AFILIACION = aFILIACION;
	}

	public String getUSUARIO() {
		return USUARIO;
	}

	public void setUSUARIO(String uSUARIO) {
		USUARIO = uSUARIO;
	}

	public String getCLAVE_USUARIO() {
		return CLAVE_USUARIO;
	}

	public void setCLAVE_USUARIO(String cLAVE_USUARIO) {
		CLAVE_USUARIO = cLAVE_USUARIO;
	}

	public String getMERCHANT_ID() {
		return MERCHANT_ID;
	}

	public void setMERCHANT_ID(String mERCHANT_ID) {
		MERCHANT_ID = mERCHANT_ID;
	}

	public String getID_TERMINAL() {
		return ID_TERMINAL;
	}

	public void setID_TERMINAL(String iD_TERMINAL) {
		ID_TERMINAL = iD_TERMINAL;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getCOMERCIO() {
		return COMERCIO;
	}

	public void setCOMERCIO(String cOMERCIO) {
		COMERCIO = cOMERCIO;
	}

	public int getACTIVO() {
		return ACTIVO;
	}

	public void setACTIVO(int aCTIVO) {
		ACTIVO = aCTIVO;
	}

	public String getBANCO() {
		return BANCO;
	}

	public void setBANCO(String bANCO) {
		BANCO = bANCO;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getSTORE() {
		return STORE;
	}

	public void setSTORE(String sTORE) {
		STORE = sTORE;
	}

	public String getFORWARD_PATH() {
		return FORWARD_PATH;
	}

	public void setFORWARD_PATH(String fORWARD_PATH) {
		FORWARD_PATH = fORWARD_PATH;
	}

}
