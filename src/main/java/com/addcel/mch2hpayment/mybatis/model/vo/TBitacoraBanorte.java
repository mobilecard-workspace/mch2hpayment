package com.addcel.mch2hpayment.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TBitacoraBanorte {

	private long id_usuario;
	private long id_bitacora;
	private long transaction_number;
	private long id_card;
	private  long account_id;
	private double amount; 
	private String idioma;
	private String bank_reference;
	private double comision;
	private String texto;
	private String referencia;
	private String resultado_payw;
	private String resultado_aut;
	private String banco_emisor;
	private String tipo_tarjeta;
	private String marca_tarjeta;
	private String codigo_aut;
	private String status3DS;
	private String concepto;
	private String tarjeta_transferencia;
	private String nombre_destino;
	private String banco_destino;
	private String cuenta_clabe_destino;
	private int id_aplicacion;
	private double lat;
	private double lon;
	
	public TBitacoraBanorte() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public double getLat() {
		return lat;
	}



	public void setLat(double lat) {
		this.lat = lat;
	}



	public double getLon() {
		return lon;
	}



	public void setLon(double lon) {
		this.lon = lon;
	}



	public void setId_aplicacion(int id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}
	
	public int getId_aplicacion() {
		return id_aplicacion;
	}
	
	public String getNombre_destino() {
		return nombre_destino;
	}



	public void setNombre_destino(String nombre_destino) {
		this.nombre_destino = nombre_destino;
	}



	public String getBanco_destino() {
		return banco_destino;
	}



	public void setBanco_destino(String banco_destino) {
		this.banco_destino = banco_destino;
	}



	public String getCuenta_clabe_destino() {
		return cuenta_clabe_destino;
	}



	public void setCuenta_clabe_destino(String cuenta_clabe_destino) {
		this.cuenta_clabe_destino = cuenta_clabe_destino;
	}



	public String getConcepto() {
		return concepto;
	}



	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}



	public String getTarjeta_transferencia() {
		return tarjeta_transferencia;
	}



	public void setTarjeta_transferencia(String tarjeta_transferencia) {
		this.tarjeta_transferencia = tarjeta_transferencia;
	}



	public void setStatus3DS(String status3ds) {
		status3DS = status3ds;
	}
	
	public String getStatus3DS() {
		return status3DS;
	}
	
	public String getTexto() {
		return texto;
	}



	public void setTexto(String texto) {
		this.texto = texto;
	}



	public String getReferencia() {
		return referencia;
	}



	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}



	public String getResultado_payw() {
		return resultado_payw;
	}



	public void setResultado_payw(String resultado_payw) {
		this.resultado_payw = resultado_payw;
	}



	public String getResultado_aut() {
		return resultado_aut;
	}



	public void setResultado_aut(String resultado_aut) {
		this.resultado_aut = resultado_aut;
	}



	public String getBanco_emisor() {
		return banco_emisor;
	}



	public void setBanco_emisor(String banco_emisor) {
		this.banco_emisor = banco_emisor;
	}



	public String getTipo_tarjeta() {
		return tipo_tarjeta;
	}



	public void setTipo_tarjeta(String tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}



	public String getMarca_tarjeta() {
		return marca_tarjeta;
	}



	public void setMarca_tarjeta(String marca_tarjeta) {
		this.marca_tarjeta = marca_tarjeta;
	}



	public String getCodigo_aut() {
		return codigo_aut;
	}



	public void setCodigo_aut(String codigo_aut) {
		this.codigo_aut = codigo_aut;
	}



	public void setComision(double comision) {
		this.comision = comision;
	}
	
	public double getComision() {
		return comision;
	}

	public void setBank_reference(String bank_reference) {
		this.bank_reference = bank_reference;
	}
	
	public String getBank_reference() {
		return bank_reference;
	}
	
	public long getId_card() {
		return id_card;
	}



	public void setId_card(long id_card) {
		this.id_card = id_card;
	}



	public long getAccount_id() {
		return account_id;
	}



	public void setAccount_id(long account_id) {
		this.account_id = account_id;
	}



	public double getAmount() {
		return amount;
	}



	public void setAmount(double amount) {
		this.amount = amount;
	}



	public String getIdioma() {
		return idioma;
	}



	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}



	public long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public long getId_bitacora() {
		return id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	public long getTransaction_number() {
		return transaction_number;
	}

	public void setTransaction_number(long transaction_number) {
		this.transaction_number = transaction_number;
	}
	
	
	
}
