package com.addcel.mch2hpayment.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.mybatis.model.vo.Afiliacion;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;







public interface ServiceMapper {

	/*public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public List<servicios> getServicios(String estados);
	public int dataCheck(long id_usuario);
	public void DetailInsert(AciDetail detail);
	public int CvvCheck(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public User getUserData(long id_usuario);
	public void updateUser(User user);
	public String getCt(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public Proveedor getProveedor(String proveedor);
	public servicios getAddreses(int id_servicio);
	public Card getMobilecardCard(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public String getEstado(@Param(value = "usr_id_estado")int usr_id_estado);
	public String getParameter(@Param(value = "parameter") String parameter);*/
	
	public void addBitacora(TBitacoraVO bitacora);
	public Card getCard(@Param(value = "idUsuario") long idUsuario, @Param(value = "idTarjeta") long idTarjeta);
	public long getMaxIdBitacora();
	//public String H2HBanorte_Business(@Param(value = "idUsuario") long idUsuario , @Param(value = "idBitacora") long idBitacora, @Param(value = "transactionNumber") long transactionNumber, @Param(value = "alias") String alias, @Param(value = "proceso") int proceso , @Param(value = "nidioma") String nidioma);
	public String H2HBanorte_Business(@Param(value = "idUsuario") long idUsuario , @Param(value = "idBitacora") long idBitacora, @Param(value = "transactionNumber") long transactionNumber, @Param(value = "alias") String alias, @Param(value = "proceso") int proceso , @Param(value = "idAccount") long idAccount ,@Param(value = "telefono") String telefono,@Param(value = "correo") String correo,@Param(value = "nidioma") String nidioma);
	
	public AccountRecord getAccount(@Param(value = "idaccount") long idaccount);
	public User getUserData(@Param(value = "idUsuario") long idUsuario);
	public List<BankCodes> getBankcodes(); 
	public BankCodes getBankcodesByClave(@Param(value = "clave_banco") String clave_banco);
	public TBitacoraBanorte getBitacoraBanorte(@Param(value = "idBitacora") long idBitacora);
	public void insertTbitacoraMCBanorte(TBitacoraBanorte bitacora);
	public void  updateTbitacora(@Param(value = "idBitacora") long idBitacora,@Param(value = "status") int status, @Param(value = "aut") String aut);
	public void updateBitacoraBanorte(@Param(value = "transaction") long transaction,@Param(value = "idBitacora") long idBitacora, @Param(value = "reference") String reference);
	public void updateBitacoraBanorteOBJ(TBitacoraBanorte bitacora);
	public void updateuserSMS(@Param(value = "idUsuario") long idUsuario);
	public Afiliacion getAfiliacion(@Param(value = "idAfiliacion") long idAfiliacion);
	public AfiliacionVO buscaAfiliacion(@Param(value = "banco")String banco);
	public String getParameter(@Param(value = "parameter") String parameter);
	public Proveedor getProveedor(String proveedor);
	public int GetAmountUserTransfer(@Param(value = "idUsuario") long idUsuario);
	public int isBinValido(@Param(value = "bin")String bin);
	public String RulesH2HBanorte(@Param(value = "idUser") long idUser,@Param(value = "accountId") long accountId, @Param(value = "cardId") long cardId,@Param(value = "amount_transfer") Double amount_transfer,@Param(value = "bin") String bin,@Param(value = "idioma") String idioma);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
}
