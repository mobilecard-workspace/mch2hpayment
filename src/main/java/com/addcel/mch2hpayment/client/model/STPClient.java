package com.addcel.mch2hpayment.client.model;

public interface STPClient {

	public MoneySendRes moneySend(MoneySendReq req, int idApp, int idPais, String idioma);
}
