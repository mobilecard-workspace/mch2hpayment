package com.addcel.mch2hpayment.client.model;

public interface BillPocketClient {
	
	public BillPocketResponse AutorizationManual(AutorizationManualRequest request,Integer idApp,Integer idPais, String idioma);
	public BillPocketResponse Refund(long idTransaccion, Integer idApp,Integer idPais, String idioma);

}
