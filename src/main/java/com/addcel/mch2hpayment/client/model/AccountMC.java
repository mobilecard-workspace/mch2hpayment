package com.addcel.mch2hpayment.client.model;

public class AccountMC {
    
	private long id;
	private String holder_name;
	private String rfc;
	private String phone;
	private String contact;
	private String email;
	private String act_type; // clabe
	private String bank_code; //aqui gaurdar el nuevo valor
	private String act_number;
	private String banorte_clave_id;
	private int status;
	
	public AccountMC() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHolder_name() {
		return holder_name;
	}

	public void setHolder_name(String holder_name) {
		this.holder_name = holder_name;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAct_type() {
		return act_type;
	}

	public void setAct_type(String act_type) {
		this.act_type = act_type;
	}

	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getAct_number() {
		return act_number;
	}

	public void setAct_number(String act_number) {
		this.act_number = act_number;
	}

	public String getBanorte_clave_id() {
		return banorte_clave_id;
	}

	public void setBanorte_clave_id(String banorte_clave_id) {
		this.banorte_clave_id = banorte_clave_id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
