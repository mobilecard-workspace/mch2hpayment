package com.addcel.mch2hpayment.client.model;

public interface ThreatMetrixClient {

	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request);
}
