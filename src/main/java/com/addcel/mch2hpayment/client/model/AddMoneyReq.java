package com.addcel.mch2hpayment.client.model;

public class AddMoneyReq {
	
	private Long idUsuario;
	private Double Cantidad;
	private String Tarjeta;
	
	public AddMoneyReq() {
		// TODO Auto-generated constructor stub
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}
	
	public Double getCantidad() {
		return Cantidad;
	}

	public void setCantidad(Double cantidad) {
		Cantidad = cantidad;
	}

	public String getTarjeta() {
		return Tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		Tarjeta = tarjeta;
	}
	
	

}
