package com.addcel.mch2hpayment.client.model;

public class DataPreviVale {

	private String authorization;
	private Integer previvaleCode;
	private Integer idPeticion;
	
	public DataPreviVale() {
		// TODO Auto-generated constructor stub
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public Integer getPrevivaleCode() {
		return previvaleCode;
	}

	public void setPrevivaleCode(Integer previvaleCode) {
		this.previvaleCode = previvaleCode;
	}

	public Integer getIdPeticion() {
		return idPeticion;
	}

	public void setIdPeticion(Integer idPeticion) {
		this.idPeticion = idPeticion;
	}
	
	
	
	
	
}
