package com.addcel.mch2hpayment.client.model;

public class ValidateTokenResponse {
	
	private Integer code;
	private String message;
	
	public ValidateTokenResponse() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
