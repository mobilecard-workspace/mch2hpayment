package com.addcel.mch2hpayment.client.model;

public class AddMoneyRes {

	private Integer code;
	private String message;
	DataPreviVale data;
	
	public AddMoneyRes() {
		// TODO Auto-generated constructor stub
	}
	
	public AddMoneyRes(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DataPreviVale getData() {
		return data;
	}

	public void setData(DataPreviVale data) {
		this.data = data;
	}
	
	
	
}
