package com.addcel.mch2hpayment.client.model;

public class MoneySendReq {
	
	private Long idUsuario;
	private String conceptoPago;
	private String cuentaBeneficiario;
	private String institucionContraparte;
	private String monto;
	private String nombreBeneficiario;
	private String tipoCuentaBeneficiario;
	
	public MoneySendReq() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getConceptoPago() {
		return conceptoPago;
	}

	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public String getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getInstitucionContraparte() {
		return institucionContraparte;
	}

	public void setInstitucionContraparte(String institucionContraparte) {
		this.institucionContraparte = institucionContraparte;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	public String getMonto() {
		return monto;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getTipoCuentaBeneficiario() {
		return tipoCuentaBeneficiario;
	}

	public void setTipoCuentaBeneficiario(String tipoCuentaBeneficiario) {
		this.tipoCuentaBeneficiario = tipoCuentaBeneficiario;
	}
	

}
