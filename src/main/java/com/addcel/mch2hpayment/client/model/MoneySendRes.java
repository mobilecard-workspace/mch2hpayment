package com.addcel.mch2hpayment.client.model;

public class MoneySendRes {
	
	private Integer code;
	private String message;
	private Integer idOperacion;
	private String claveRastreo;
	private String folio;
	private String referencia;
	private String idStpTransaccionesDispersion;
	private String idPeticionWsStp;
	
	
	public MoneySendRes() {
		// TODO Auto-generated constructor stub
	}


	public Integer getCode() {
		return code;
	}


	public void setCode(Integer code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Integer getIdOperacion() {
		return idOperacion;
	}


	public void setIdOperacion(Integer idOperacion) {
		this.idOperacion = idOperacion;
	}


	public String getClaveRastreo() {
		return claveRastreo;
	}


	public void setClaveRastreo(String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}


	public String getFolio() {
		return folio;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getReferencia() {
		return referencia;
	}


	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	public String getIdStpTransaccionesDispersion() {
		return idStpTransaccionesDispersion;
	}


	public void setIdStpTransaccionesDispersion(String idStpTransaccionesDispersion) {
		this.idStpTransaccionesDispersion = idStpTransaccionesDispersion;
	}


	public String getIdPeticionWsStp() {
		return idPeticionWsStp;
	}


	public void setIdPeticionWsStp(String idPeticionWsStp) {
		this.idPeticionWsStp = idPeticionWsStp;
	}
	

}
