package com.addcel.mch2hpayment.spring.model;

public class ProcomVO {
	
	// Atributos obligatorios
		private String total;
		private String currency;
		private String address;
		private String orderId;
		private String merchant;
		private String store;
		private String term;
		private String digest;
		private String urlBack;

		private String monto;
		private String idServicio;
		
		
		private String nombre; 
		private String TDC;
		private String tipoTDC;
		private String mes;
		private String anio;
		private String cc_numberback;
		private String cc_cvv;
		
		public ProcomVO(String total, String currency, String address, String orderId, String merchant, String store, String term, String digest, String urlBack,String nombre, String TDC,String tipoTDC,String mes,String anio, String cc_numberback,String cc_cvv) {
			this.total = total;
			this.currency = currency;
			this.address = address;
			this.orderId = orderId;
			this.merchant = merchant;
			this.store = store;
			this.term = term;
			this.digest = digest;
			this.urlBack = urlBack;
			this.nombre = nombre;
			this.TDC = TDC;
			this.tipoTDC = tipoTDC;
			this.mes = mes;
			this.anio = anio;
			this.cc_numberback = cc_numberback;
			this.cc_cvv = cc_cvv;
		}
		
		public ProcomVO() {
			// TODO Auto-generated constructor stub
		}
		
		
		public void setCc_cvv(String cc_cvv) {
			this.cc_cvv = cc_cvv;
		}
		
		public String getCc_cvv() {
			return cc_cvv;
		}
		
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getTDC() {
			return TDC;
		}

		public void setTDC(String tDC) {
			TDC = tDC;
		}

		public String getTipoTDC() {
			return tipoTDC;
		}

		public void setTipoTDC(String tipoTDC) {
			this.tipoTDC = tipoTDC;
		}

		public String getMes() {
			return mes;
		}

		public void setMes(String mes) {
			this.mes = mes;
		}

		public String getAnio() {
			return anio;
		}

		public void setAnio(String anio) {
			this.anio = anio;
		}

		public String getCc_numberback() {
			return cc_numberback;
		}

		public void setCc_numberback(String cc_numberback) {
			this.cc_numberback = cc_numberback;
		}

		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getMerchant() {
			return merchant;
		}
		public void setMerchant(String merchant) {
			this.merchant = merchant;
		}
		public String getStore() {
			return store;
		}
		public void setStore(String store) {
			this.store = store;
		}
		public String getTerm() {
			return term;
		}
		public void setTerm(String term) {
			this.term = term;
		}
		public String getDigest() {
			return digest;
		}
		public void setDigest(String digest) {
			this.digest = digest;
		}
		public String getUrlBack() {
			return urlBack;
		}
		public void setUrlBack(String urlBack) {
			this.urlBack = urlBack;
		}
		public String getMonto() {
			return monto;
		}
		public void setMonto(String monto) {
			this.monto = monto;
		}
		public String getIdServicio() {
			return idServicio;
		}
		public void setIdServicio(String idServicio) {
			this.idServicio = idServicio;
		}
		
		

}
