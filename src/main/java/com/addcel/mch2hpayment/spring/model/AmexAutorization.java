package com.addcel.mch2hpayment.spring.model;

public class AmexAutorization {
	
	private double monto;
	private double comision;
	private String tarjeta;
	private String vigencia;
	private String cvv2;
	private String dom_amex;
	private String afiliacion;
	private String producto;
	
	public AmexAutorization() {
		// TODO Auto-generated constructor stub
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getDom_amex() {
		return dom_amex;
	}

	public void setDom_amex(String dom_amex) {
		this.dom_amex = dom_amex;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}
	
	

}
