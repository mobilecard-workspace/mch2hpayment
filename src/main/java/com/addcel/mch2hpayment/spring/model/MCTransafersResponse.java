package com.addcel.mch2hpayment.spring.model;

public class MCTransafersResponse extends McBaseResponse {

	private double montoTransfer;
	private double comision;
	private String authProcom;
	private String referenceBanorte;
	private String refProcom;
	private String tarjeta;
	private String fecha;
	
	public MCTransafersResponse() {
		// TODO Auto-generated constructor stub
	}

	public void setRefProcom(String refProcom) {
		this.refProcom = refProcom;
	}
	
	public String getRefProcom() {
		return refProcom;
	}
	
	public double getMontoTransfer() {
		return montoTransfer;
	}

	public void setMontoTransfer(double montoTransfer) {
		this.montoTransfer = montoTransfer;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getAuthProcom() {
		return authProcom;
	}

	public void setAuthProcom(String authProcom) {
		this.authProcom = authProcom;
	}

	public String getReferenceBanorte() {
		return referenceBanorte;
	}

	public void setReferenceBanorte(String referenceBanorte) {
		this.referenceBanorte = referenceBanorte;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
}
