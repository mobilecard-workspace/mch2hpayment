package com.addcel.mch2hpayment.spring.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MCSignUpResponse extends McBaseResponse{

	private List<FromAccount> accounts;
	
	public MCSignUpResponse() {
		// TODO Auto-generated constructor stub
		this.accounts = new ArrayList<FromAccount>();
	}
	
	public void setAccounts(List<FromAccount> accounts) {
		this.accounts = accounts;
	}
	
	public List<FromAccount> getAccounts() {
		return accounts;
	}
	
}
