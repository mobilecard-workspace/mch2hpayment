package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class McBaseResponse {
	
	private int idError ;
	private String mensajeError;
	
	public McBaseResponse() {
		
	}
	
	public McBaseResponse(int idError, String mensajeError) {
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	

}
