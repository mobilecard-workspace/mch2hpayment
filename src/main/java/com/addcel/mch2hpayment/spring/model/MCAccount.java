package com.addcel.mch2hpayment.spring.model;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MCAccount extends AccountRecord{

	private long idUsuario;
	private String idioma;
	private String alias;
	
	public MCAccount() {
		// TODO Auto-generated constructor stub
	}
	
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	public String getAlias() {
		return alias;
	}
	
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	public String getIdioma() {
		return idioma;
	}
	
}
