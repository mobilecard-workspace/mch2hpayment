package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest {
	
	private long idUser;
	private long idCard;
	private long accountId;
	private double amount; 
	private String concept;
	private String idioma;
	private String authProcom;
	private String refProcom;
	private double comision;
	
	public PaymentRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public void setComision(double comision) {
		this.comision = comision;
	}
	
	public double getComision() {
		return comision;
	}
	
	public void setAuthProcom(String authProcom) {
		this.authProcom = authProcom;
	}
	
	public String getAuthProcom() {
		return authProcom;
	}
	
	public void setRefProcom(String refProcom) {
		this.refProcom = refProcom;
	}
	
	public String getRefProcom() {
		return refProcom;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public long getIdCard() {
		return idCard;
	}

	public void setIdCard(long idCard) {
		this.idCard = idCard;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

}
