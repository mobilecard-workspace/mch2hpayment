package com.addcel.mch2hpayment.spring.model;

public class BPPaymentResponse {
	
	private Integer status;
	
	private Integer code;

    private Integer opId;

    private String txnISOCode;

    private String authNumber;

    private String ticketUrl;

    private double amount;

    private String maskedPAN;

    private long dateTime;
    
    private long idTransaccion;
    
    private String message;
    
    public void setCode(Integer code) {
		this.code = code;
	}
    
    public Integer getCode() {
		return code;
	}
    
    public BPPaymentResponse() {
		// TODO Auto-generated constructor stub
	}
    
    public void setMessage(String message) {
		this.message = message;
	}
    
    public String getMessage() {
		return message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getOpId() {
		return opId;
	}

	public void setOpId(Integer opId) {
		this.opId = opId;
	}

	public String getTxnISOCode() {
		return txnISOCode;
	}

	public void setTxnISOCode(String txnISOCode) {
		this.txnISOCode = txnISOCode;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	public String getTicketUrl() {
		return ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		this.ticketUrl = ticketUrl;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMaskedPAN() {
		return maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		this.maskedPAN = maskedPAN;
	}

	public long getDateTime() {
		return dateTime;
	}

	public void setDateTime(long dateTime) {
		this.dateTime = dateTime;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
    

}
