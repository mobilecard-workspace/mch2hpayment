package com.addcel.mch2hpayment.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mch2hpayment.client.model.TokenClientMC;
import com.addcel.mch2hpayment.client.model.ValidateTokenRequest;
import com.addcel.mch2hpayment.client.model.ValidateTokenResponse;
import com.google.gson.Gson;

@Service
public class TokenClientMCImpl implements TokenClientMC{

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenClientMCImpl.class);
	
	private final static String PATH_VALIDATE_TOKEN = "http://localhost:80/Tokenizer/{idApp}/{idPais}/{idioma}/validateToken";
	
	private Gson GSON = new Gson();
	
	@Override
	public ValidateTokenResponse validateToken(ValidateTokenRequest req , Integer idApp, Integer idPais, String idioma, String token) {
		// TODO Auto-generated method stub
		ValidateTokenResponse response = new ValidateTokenResponse();
		try{
			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", token);
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<ValidateTokenRequest> request = new HttpEntity<>(req,headers);
	        ResponseEntity<String> respBP = restTemplate.exchange(PATH_VALIDATE_TOKEN.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{idioma}", idioma),HttpMethod.POST, request, String.class);
	        LOGGER.info("RESPUESTA DE VALIDATE TOKEN - {}", respBP.getBody());
	        response = GSON.fromJson(respBP.getBody(), ValidateTokenResponse.class);
		}catch(Exception ex){
			LOGGER.error("ERROR AL VALIDAR TOKEN",ex);
			response.setCode(1000);
			response.setMessage("NO SE PUDO COMPLETAR LA VALIDACION DEL TOKEN");
		}
		
		return response;
	}
	
	

}
