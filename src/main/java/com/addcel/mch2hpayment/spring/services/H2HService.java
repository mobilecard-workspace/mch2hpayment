package com.addcel.mch2hpayment.spring.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.mch2hpayment.client.model.AccountMC;
import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueueAccountSignUpRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryRequest;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.FromAccount;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.utils.Business;
import com.google.gson.Gson;

@Service
public class H2HService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(H2HService.class);
	
	private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	private Gson gson = new Gson();
	@Autowired
	private H2HMapper mapper;
	
	
	public MCSignUpResponse EnqueueAccountSignUp(MCAccount mcAccount, int idApp){
		MCSignUpResponse response = new MCSignUpResponse();
		
		try{
			LOGGER.debug("RESGISTRANDO CUENTA: " + gson.toJson(mcAccount) + "   : " +idApp); 
			AccountMC acc = new AccountMC();
			acc.setAct_number(mcAccount.getActNumber().trim());
			acc.setAct_type(mcAccount.getActType().trim());
			acc.setBank_code(mcAccount.getBankCode().trim());
			acc.setBanorte_clave_id("");
			acc.setContact(mcAccount.getContact()== null ? "" : Business.removeAcentos(mcAccount.getContact()).trim());
			acc.setEmail(mcAccount.getEmail().trim());
			acc.setHolder_name(Business.removeAcentos(mcAccount.getHolderName()).trim());
			acc.setPhone(mcAccount.getPhone()== null ? "" :mcAccount.getPhone().trim());
			acc.setRfc(mcAccount.getRfc().trim());
			acc.setStatus(1);
			mapper.insertAccount(acc);
			String responsBD =  mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0, acc.getId(), mcAccount.getAlias(), 2,0,"","", mcAccount.getIdioma(),idApp);
			response = gson.fromJson(responsBD, MCSignUpResponse.class);
			LOGGER.debug("[RESPONSE BD] " +  responsBD);
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR AL REGISTAR CUENTA USUARIO]", ex);
			response.setIdError(400);
			if(mcAccount.getIdioma().equals("es"))
				response.setMensajeError("Error al procesar datos, contacte a soporte.");
			else
				response.setMensajeError("Error processing data, contact support.");
			return response;
		}
		/*try{
			
			
			Calendar calendario = new GregorianCalendar();
			int hora =calendario.get(Calendar.HOUR_OF_DAY);
			
			
			//checar horario
			if( hora >= 22 || (hora >= 0 && hora <2)){
				LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				response.setAccounts(new ArrayList<FromAccount>());
				response.setIdError(88);
				if(mcAccount.getIdioma().equals("es"))
					response.setMensajeError("Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				else
					response.setMensajeError("Service available during the hours of 2:00 to 22:00.");
				
				return response;
			}
			
			
			int sizeNumber = mcAccount.getActNumber().length();
			
			int bin = mapper.isBinValido(mcAccount.getActNumber().substring(0,6));
			
			if(bin == 1 && sizeNumber != 10 && sizeNumber != 11 && sizeNumber != 18 ){
				response.setAccounts(new ArrayList<FromAccount>());
				response.setIdError(89);
				if(mcAccount.getIdioma().equals("es"))
					response.setMensajeError("El número de tarjeta no se acepta, favor de introducir número de cuenta o clabe");
				else
					response.setMensajeError("The card number not accepted, please enter account number or clabe");
				LOGGER.info("[DETECTAMOS BIN DE TARJETA] ");
				
				return response;
			}
			
			if(mcAccount.getBankCode().equals("072") || mcAccount.getBankCode().equals("032") )
			{
				
				if(sizeNumber > 11 )
				{
					response.setAccounts(new ArrayList<FromAccount>());
					response.setIdError(90);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Para banco Banorte, introduzca el número de cuenta");
					else
						response.setMensajeError("For Banorte bank, enter the account number");
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");
					
					return response;
				}else{
					if( mcAccount.getActType().equals("CLABE") && (sizeNumber == 10 || sizeNumber == 11))
						mcAccount.setActType("ACT");
				}
				
				
			}
			else{
				if(sizeNumber < 18){
					response.setAccounts(new ArrayList<FromAccount>());
					response.setIdError(91);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Para bancos distintos a Banorte, introduzca la clabe interbancaria");
					else
						response.setMensajeError("For banks other than Banorte, enter the interbank clabe");
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");
					return response;
				}else{
					if( mcAccount.getActType().equals("ACT") && sizeNumber == 18)
						mcAccount.setActType("CLABE");
				}
			}
			
			//verificar clave banco con cuenta
			if( mcAccount.getActType().equals("CLABE")){
				if(!mcAccount.getActNumber().substring(0, 3).equals(mcAccount.getBankCode())){
					response.setIdError(92);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Verifique que la cuenta introducida pertenezca al banco seleccionado.");
					else
						response.setMensajeError("Verify that the account entered belongs to the selected bank.");
					LOGGER.info("[DETECTAMOS CUENTA INCONSISTENTE CODIGOBANK vs NUMCUENTA] " );
					return response;
				}
			}
			
			login = login();
			EnqueueAccountSignUpRequest signup = new EnqueueAccountSignUpRequest();
			long clientReference = mapper.getMaxIdBitacora();
			
			if(mcAccount.getRfc().length() > 13)
				mcAccount.setRfc(mcAccount.getRfc().substring(0, 13));
			
			if(mcAccount.getActType().equals("ACT") && mcAccount.getActNumber().length() > 11)
				mcAccount.setActType("CLABE");
			
			
			AccountRecord account = new AccountRecord();
			account.setActNumber(mcAccount.getActNumber().trim());
			account.setActType(mcAccount.getActType().trim());
			account.setBankCode(mcAccount.getBankCode().trim());
			account.setContact(mcAccount.getContact()== null ? "" : Business.removeAcentos(mcAccount.getContact()).trim() );//opcional
			account.setEmail(mcAccount.getEmail().trim());
			account.setHolderName(Business.removeAcentos(mcAccount.getHolderName()).trim()); //obligatorio
			account.setPhone(mcAccount.getPhone()== null ? "" :mcAccount.getPhone().trim() ); //opcional
			int sizerfc = mcAccount.getRfc().trim().length() -1;
			account.setRfc(mcAccount.getRfc().trim().substring(0,sizerfc)+idApp);
			signup.setAccount(account);
			signup.setClientReference(new Date().getSeconds()+""+clientReference); //verificar al final
			signup.setIdAccount(""); // opcional Banorte genera uno
			signup.setToken(login.getToken());
			//LOGGER.info("Request alta cuenta " + gson.toJson(signup));
			EnqueueTransactionResponse responseBanorte = h2hclient.EnqueueAccountSignUp(signup, mcAccount.getIdioma());
			LOGGER.debug("RESPUESTA ALTA " + responseBanorte.getResult().isSuccess() + " " + gson.toJson(responseBanorte));
			if(responseBanorte.getResult().isSuccess()){
				String responsBD =  mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0, Long.parseLong(responseBanorte.getTransactionReference()), mcAccount.getAlias(), 2,0,"","", mcAccount.getIdioma(),idApp);
				response = gson.fromJson(responsBD, MCSignUpResponse.class);
				//LOGGER.debug("[RESPONSE BD] " +  responsBD);
			}else
			{
				response.setIdError(10);
				response.setMensajeError(responseBanorte.getResult().getMessage());
			}
			logout();
			return response;
			
		}
		catch(Exception ex){
			LOGGER.error("[ERROR AL REGISTAR CUENTA USUARIO]", ex);
			return response;
		}*/
	}
	
	public MCSignUpResponse getToAccount(long idUsuario, String idioma, int idApp){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			//login = login();
			String responseBD = mapper.H2HBanorte_Business(idUsuario, 0, 0, "", 3,0,"" ,"",idioma, idApp);
			//LOGGER.debug("[RESPUESTA TO ACCOUNT] " + responseBD);
			response = gson.fromJson(responseBD, MCSignUpResponse.class);
			LOGGER.debug("RETORNANDO CUENTAS: " + gson.toJson(response));
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER CUANTAS ASOCIADAS ] ["+idUsuario+"]", ex);
		}finally{
			//logout();
		}
		return response;
	}
	
	public LoginResponse login(){
		try{
			LoginRequest login  = new LoginRequest();
			login.setLogin("addcel_banorte");
			login.setPassword("B4N0rt3%");
			LOGGER.debug("iniciando login");
			LoginResponse response = h2hclient.login(login, "es");
			LOGGER.debug("LOGUIN: "+  response.getToken() + " " + response.getResult().isSuccess() + " " + response.getResult().getMessage());
			return response;
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			return new  LoginResponse();
		}
	}
	
	public LogoutResponse logout(){
		try{
			LogoutRequest logout = new LogoutRequest();
			logout.setLogin("addcel_banorte");
			logout.setToken(login.getToken());
			LogoutResponse response = h2hclient.logout(logout, "es");
			LOGGER.debug("[RESPUESTA DE LOGOUT]" +  response.getResult().isSuccess());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR EN LOGOUT]", ex);
			return new LogoutResponse();
		}
	}
	
	
	/**
	 * Actualiza cuenta destino
	 * @param request
	 * @return
	 */
	public MCSignUpResponse updateAccount(UpdateAccountRequest request, int idApp){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			LOGGER.debug("[INICIANDO ACTUALIZACION CUENTA ]" + gson.toJson(request));
			String responseDB = mapper.H2HBanorte_Business(request.getIdUsuario(), 0, 0, request.getAlias().trim(), 4, request.getIdAccount(),request.getTelefono().trim(), request.getEmail().trim() ,request.getIdioma().trim(), idApp); // proceso 4
			//LOGGER.debug("RESPONSE JSON: " + responseDB);
			response = gson.fromJson(responseDB, MCSignUpResponse.class);
		}catch(Exception ex){
			response.setIdError(99);
			if(request.getIdioma().equalsIgnoreCase("es"))
				response.setMensajeError("Error inesperado, vuelva a intentarlo");
			else
				response.setMensajeError("Unexpected error, try again");
			LOGGER.error("[ERROR AL ACTUALIZAR CUENTA DESTINO ] " + gson.toJson(request), ex);
		}
		LOGGER.debug("RETORNANDO RESPUESTA : " + gson.toJson(response));
		return response;
	}
	
	
	public MCSignUpResponse deleteAccount(long idUsuario, long idAccount, String idioma, int idApp){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			LOGGER.debug("[INICIANDO ELIMINACION DE CUENTA ]" + idUsuario);
			String responseDB = mapper.H2HBanorte_Business(idUsuario, 0, 0, "", 5,idAccount,"","",idioma, idApp); // proceso 5
		//	LOGGER.debug("RESPONSE JSON: " + responseDB);
			response = gson.fromJson(responseDB, MCSignUpResponse.class);
		}catch(Exception ex){
			if(idioma.equalsIgnoreCase("es"))
				response.setMensajeError("Error inesperado, vuelva a intentarlo");
			else
				response.setMensajeError("Unexpected error, try again");
			LOGGER.error("[ERROR AL ELIMINAR CUENTA DESTINO ] " + idUsuario + "-" + idAccount , ex);
		}
		LOGGER.debug("RETORNANDO RESPUESTA: " + gson.toJson(response));
		return response;
	}
	
	/*
	 * Realiza la consulta del status de una transacción encolada en el H2H Banorte.
	 */
	public TransactionEnquiryResponse TransactionEnquiry(String transactionReference, String idioma, int idApp ){
		try{
			login = login();
			TransactionEnquiryRequest request = new TransactionEnquiryRequest();
			request.setToken(login.getToken());
			request.setTransactionReference(transactionReference);
			LOGGER.debug("[CONSULTANDO ESTATUS DE TRANSACCION] " + request.getTransactionReference());
			TransactionEnquiryResponse  response = h2hclient.TransactionEnquiry(request,idioma);
			LOGGER.debug("[FINALIZANDO CONSULTA DE ESTATUS]" + request.getTransactionReference());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR AL CONSULTAR ESTATUS DE TRANSACCION] " + transactionReference, ex);
			return null;
		}finally{
			logout();
		}
	}
	
	public BankCodesResponse getBankCodes(int idApp){
		BankCodesResponse response = new BankCodesResponse();
		try{
			LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS]");
			List<Bank> catalogo = mapper.getCatalogoBancos();
			List<BankCodes>  bankCa= new ArrayList<BankCodes>();
			
			for(Bank bank : catalogo)
			{
				BankCodes b = new BankCodes();
				b.setClave(bank.getClave()+"");
				b.setComision_fija(0.0);
				b.setComision_porcentaje(0.0);
				b.setId(bank.getId());
				b.setNombre_corto(bank.getDescripcion());
				b.setNombre_razon_social(bank.getDescripcion());
				bankCa.add(b);
			}
			
			response.setBanks(bankCa);
			response.setIdError(0);
			response.setMensajeError("");
			LOGGER.debug("LISTA DE BANCOS: " + gson.toJson(response));
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
			response.setIdError(99);
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		}
		return response;
	}
	
}
