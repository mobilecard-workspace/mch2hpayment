package com.addcel.mch2hpayment.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mch2hpayment.client.model.AddMoneyReq;
import com.addcel.mch2hpayment.client.model.AddMoneyRes;
import com.addcel.mch2hpayment.client.model.PrevivaleClient;
import com.google.gson.Gson;

@Service
public class PrevivaleClientImp  implements PrevivaleClient{

	private static final Logger LOGGER = LoggerFactory.getLogger(PrevivaleClientImp.class);
	
	private static  String SERVER = "http://localhost:80/"; // prod
	private static final String URL_AGREGA_DINERO = SERVER + "PreviVale/api/@idApp/@idPais/@idioma/agregarDinero";

	private Gson gson = new Gson();
	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public AddMoneyRes agregarDinero(AddMoneyReq req, int idApp, int idPais,
			String idioma) {
		
		try{
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<AddMoneyReq> request = new HttpEntity<AddMoneyReq>(req,headers);
			
			LOGGER.debug("REQUEST  PREVIVALE: " +  gson.toJson(req));
			AddMoneyRes response = restTemplate.postForObject(URL_AGREGA_DINERO.replace("@idApp", idApp+"").replace("@idPais", idApp+"").replace("@idioma", idioma) ,request, AddMoneyRes.class);
			LOGGER.debug("RESPONSE PREVIVALE: " + gson.toJson(response));
			
			return response;
		}catch(Exception ex){
			LOGGER.error("ERROR AL AGREGAR DINERO", ex);
			return new AddMoneyRes(-1);
		}
	}
	
}
