package com.addcel.mch2hpayment.spring.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.TransaccionBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.AmexAutorization;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.PayworksVO;
import com.addcel.mch2hpayment.spring.model.RespuestaAmex;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PayworksH2HService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksH2HService.class);
	private String messageId;
	
	private Gson gson = new Gson();
	private SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");;
	private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	
	@Autowired
	H2HMapper mapper;
	@Autowired
	AmexService amexService;
	
	public ModelAndView send3DSBanorte(PaymentRequest request, int idApp, double lat, double lon){
		
		
		messageId = request.getIdUser()+"";
		ModelAndView view = new ModelAndView();
		AfiliacionVO afiliacion = null;
		try{
			
			Calendar calendario = new GregorianCalendar();
			int hora =calendario.get(Calendar.HOUR_OF_DAY);
			Card card = mapper.getCard(request.getIdUser(), request.getIdCard(), idApp); //
			
			
			/*if( hora >= 22 || (hora >= 0 && hora <2)){
				LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				view = new ModelAndView("payworks/pagina-error");
				view.addObject("idApp",idApp);
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				else
					view.addObject("mensajeError","Service available during the hours of 2:00 to 22:00.");
				
				return view;
			}*/
				
			
				
					
					String  rules_response = mapper.RulesH2HBanorte(request.getIdUser(), request.getAccountId(), request.getIdCard(),request.getAmount(), UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6),request.getIdioma(), idApp);
					McBaseResponse Rules = gson.fromJson(rules_response, McBaseResponse.class); 
					if(Rules.getIdError() != 0){
						LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
						view = new ModelAndView("payworks/pagina-error");
						view.addObject("idApp",idApp);
						view.addObject("mensajeError", Rules.getMensajeError());
						return view;
					}
			
			
			
			User user = mapper.getUserData(request.getIdUser(), idApp);
			if(user.getId_usr_status() != 1){
				LOGGER.info("[DETECTAMOS USUARIO INACTIVO] " + gson.toJson(request));
				view = new ModelAndView("payworks/pagina-error");
				view.addObject("idApp",idApp);
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Existen problemas con su usuario, Para aclaraciones marque 01-800-925-5001.");
				else
					view.addObject("mensajeError","There are problems with your user. For clarifications call 01-800-925-5001.");
				
				return view;
			}
			
			AccountRecord toAccount = mapper.getAccount(request.getAccountId());
			
			if(toAccount.getBankCode().equals("072") || toAccount.getBankCode().equals("032")){
				
				if(toAccount.getActType().equals("CLABE") || toAccount.getActNumber().length() > 11){
					
					//LOGGER.info("[Detectamos Cuenta banorte inconsistente] " + gson.toJson(request));
					view = new ModelAndView("payworks/pagina-error");
					view.addObject("idApp",idApp);
					if(request.getIdioma().equals("es"))
						view.addObject("mensajeError", 
								"Hemos detectado inconsistencias en la cuenta destino, favor de verificarla");
					else
						view.addObject("mensajeError","We have detected inconsistencies in the destination account, please verify it");
					
					return view;
		                 
				}
			}
			
			
			
			if(card.getEstado() == -1 ){
				LOGGER.info("[SE DETECTO UNA TARJETA BLOQUEADA] " + request.getIdUser() );
				view = new ModelAndView("payworks/pagina-error");
				view.addObject("idApp",idApp);
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "La tarjeta seleccionada se encuentra bloqueda para realizar movimientos. Para aclaraciones marque 01-800-925-5001.");
				else
					view.addObject("mensajeError","The selected card is locked to make movements. For clarifications call 01-800-925-5001.");
				
				return view;
			}
			
			
			
			BankCodes bank = mapper.getBankcodesByClave(toAccount.getBankCode());
			double comisionCalc = bank.getComision_fija() + ((Double.valueOf(request.getAmount())+bank.getComision_fija()) * bank.getComision_porcentaje());
			if(request.getComision() == 0)
				request.setComision(comisionCalc);
			/*
			account.getComisionFija() + ((Double.valueOf(monto) + account.getComisionFija())
					* account.getComisionPorcentaje());
					*/
			
			Proveedor proveedor = mapper.getProveedor("H2HBanorte");
			afiliacion = mapper.buscaAfiliacion("BANORTEH2HN");
			
			
			//Detectar tarjeta AMEX
			if(card.getIdfranquicia() == 3){
				LOGGER.info("[SE DETECTO UNA TARJETA AMEX] " + request.getIdUser() + " " + request.getIdCard());
				view = new ModelAndView("payworks/pagina-error");
				view.addObject("idApp",idApp);
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Estimado usuario, estamos trabajando para poder realizar transferencias con tarjetas American Express.");
				else
					view.addObject("mensajeError","Dear User, we are working to make transfers with American Express cards.");
				
				return view;
				//return SendAmex(user, card, request, idApp, proveedor, afiliacion, toAccount, lat, lon);
			}
			
			
			//detectar bin nacional
			int bin = mapper.isBinValido(UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6));
			
			if(bin == 0){
				LOGGER.info("[SE DETECTO UNA TARJETA NO BIN VALIDO] " + request.getIdUser() + " ");
				view = new ModelAndView("payworks/pagina-error");
				view.addObject("idApp",idApp);
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Estimado usuario, las transferencia solo aplica para tarjetas Mexicanas.");
				else
					view.addObject("mensajeError","Dear User, the transfer only applies to Mexican cards.");
				
				return view;
			}
			
			String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			String vig = UtilsService.getSMS(card.getVigencia());
			String ct = UtilsService.getSMS(card.getCt()); 
			//String mes = vig.split("/")[0];
			//String anio = "20"+vig.split("/")[1];
			LOGGER.debug("Comision calculada: " + comisionCalc);
			LOGGER.debug("comision request: " + request.getComision());
			LOGGER.debug("amount " + request.getAmount());
			String cardType = card.getIdfranquicia() == 1? "VISA" : "MC";
			double comision = request.getComision();
			BigDecimal bd = new BigDecimal(comision);
			bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			comision = bd.doubleValue();
			
			
			TBitacoraVO bitacora = new TBitacoraVO();
			bitacora.setTipo("");
			bitacora.setSoftware("");
			bitacora.setModelo("");
			bitacora.setWkey("");
			bitacora.setIdUsuario(request.getIdUser()+"");
			bitacora.setIdProducto(1);
			bitacora.setCar_id(1);
			bitacora.setCargo(request.getAmount()+comision);
			bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			bitacora.setConcepto("Transferencia H2HBANORTE("+ (idApp == 1 ? "Mobilecard" : "Telecom") +"): Concepto: " + request.getConcept() + " Monto: "
					+ request.getAmount() + " Comision: " + comision );
			bitacora.setImei("");
			bitacora.setDestino( request.getAccountId()+"" );
			bitacora.setTarjeta_compra(card.getNumerotarjeta());
			bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			bitacora.setComision(comision);
			bitacora.setCx("");
			bitacora.setCy("");
			bitacora.setPin("");
			bitacora.setId_aplicacion(idApp);
			mapper.addBitacora(bitacora);
			
			TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			bitacora_banorte.setAccount_id(request.getAccountId());
			bitacora_banorte.setAmount(request.getAmount());
			bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			bitacora_banorte.setId_card(request.getIdCard());
			bitacora_banorte.setId_usuario(request.getIdUser());
			bitacora_banorte.setIdioma(request.getIdioma());
			bitacora_banorte.setTransaction_number(0);
			bitacora_banorte.setBank_reference("");
			bitacora_banorte.setComision(comision);
			bitacora_banorte.setTexto("");
			bitacora_banorte.setReferencia("");
			bitacora_banorte.setResultado_payw("");
			bitacora_banorte.setResultado_aut("");
			bitacora_banorte.setBanco_emisor("");
			bitacora_banorte.setTipo_tarjeta("");
			bitacora_banorte.setMarca_tarjeta("");
			bitacora_banorte.setCodigo_aut("");
			bitacora_banorte.setStatus3DS("");
			bitacora_banorte.setConcepto(request.getConcept());
			bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			bitacora_banorte.setBanco_destino(toAccount.getBankCode());
			bitacora_banorte.setId_aplicacion(idApp);
			bitacora_banorte.setLat(lat);
			bitacora_banorte.setLon(lon);
			//insertando en bitacora McBanorte
			mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			
			double total = comision + request.getAmount();
			
			
			PayworksVO payVO = new PayworksVO();
			payVO.setCard(numcard);//  
			payVO.setCardType(cardType);//cardType VISA
			payVO.setExpires(vig);//vig "04/21"
			payVO.setForwardPath(afiliacion.getForwardPath().split(" ")[0]);//Constantes.URLBACK3DSPAYW
			payVO.setMerchantId(afiliacion.getMerchantId());
			payVO.setReference3D(bitacora.getIdBitacora()+"");
			payVO.setTotal(Business.formatoMontoPayworks(total+""));
			
			LOGGER.info("ENVIO DE INFO AL 3DS BANORTE"+"["+messageId+"]"+"["+bitacora.getIdBitacora()+"] " + gson.toJson(payVO));
			
			view.setViewName("payworks/comercio-send");
			view.addObject("payworks", payVO);
			view.addObject("idApp",idApp);
			
		}catch(Exception ex){
			//LOGGER.info("["+messageId+"] " + gson.toJson(request));
			LOGGER.error("[ERROR AL ENVIAR COBRO 3DS]"+"["+messageId+"]", ex);
			
			view = new ModelAndView("payworks/pagina-error");
			view.addObject("idApp",idApp);
			if(request.getIdioma().equals("es"))
				view.addObject("mensajeError", 
						"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
								+ "Por favor vuelva a intentarlo en unos minutos.");
			else
				view.addObject("mensajeError","Sorry for the inconvenience, we detected an error when authorizing your transaction. "
                 + "Please try again in a few minutes.");
		}
		
		return view;
	}
	
	
	// respuesta 3ds banorte y envio de confirmacion a banorte
		public ModelAndView payworksRec3DRespuesta(String cadena, ModelMap modelo){
			HashMap<String, Object> resp = new HashMap<String, Object>();
			ModelAndView mav = null;
			int idapp = 1;
			try{
				cadena = cadena.replace(" ", "+");
				LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
				String[] spl = cadena.split("&");
				String[] data = null;
				for(String cad: spl){
					LOGGER.debug("data:  " + cad);
					data = cad.split("=");
					resp.put(data[0], data.length >= 2? data[1]: "");
				}
				
				
				
				TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				bitacora_banorte.setId_bitacora(Long.parseLong((String)resp.get("Reference3D")));
				bitacora_banorte.setTexto( resp.get("Status") != null ?  (String)Constantes.errorPayW.get(resp.get("Status")) : "" );
				bitacora_banorte.setReferencia("");
				bitacora_banorte.setResultado_payw("");
				bitacora_banorte.setResultado_aut("");
				bitacora_banorte.setBanco_emisor("");
				bitacora_banorte.setTipo_tarjeta("");
				bitacora_banorte.setMarca_tarjeta((String)resp.get("CardType"));
				bitacora_banorte.setCodigo_aut("");
				bitacora_banorte.setBank_reference("");
				bitacora_banorte.setTransaction_number(0);
				bitacora_banorte.setStatus3DS( resp.get("Status") != null ? (String)resp.get("Status") : "");
				
				mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
				
				TBitacoraBanorte bitacoraBanorte =   mapper.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
				idapp = bitacoraBanorte.getId_aplicacion();
				
				if(resp.containsKey("Status") && "200".equals(resp.get("Status")) && resp.get("XID") != null && !((String)resp.get("XID")).isEmpty()  && resp.get("CAVV") != null && !((String)resp.get("CAVV")).isEmpty()){
					AfiliacionVO afiliacion = mapper.buscaAfiliacion("BANORTEH2HN");
					LOGGER.debug("idBitacora "+ resp.get("Reference3D").toString());
					// TBitacoraBanorte bitacoraBanorte =   mapper.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
					Card card = mapper.getCard(bitacoraBanorte.getId_usuario(), bitacoraBanorte.getId_card(), bitacoraBanorte.getId_aplicacion());
					//String CMD_TRANS = mapper.getParameter("CMD_TRANS_H2H");//TODO certificacion
					//String AUTH2H = mapper.getParameter("AUTORIZACION_H2H");//TODO 
					mav=new ModelAndView("payworks/comercioPAYW2");
					mav.addObject("payworks", resp);
					mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
					
					
				//	resp.put("AUTORIZACION_H2H",AUTH2H); //TODO
					resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
					resp.put("USUARIO", afiliacion.getUsuario());
					resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
					resp.put("CMD_TRANS", "VENTA"); //TODO PRO VENTA
					resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
					resp.put("Total", Business.formatoMontoPayworks((String)resp.get("Total")));
					resp.put("MODO", "PRD");//PRD //AUT QA
					resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
					resp.put("NUMERO_TARJETA", resp.get("Number"));
					resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
					resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));// UtilsService.getSMS(card.getCt()); 
					resp.put("MODO_ENTRADA", "MANUAL");
					resp.put("URL_RESPUESTA", afiliacion.getForwardPath().split(" ")[1]);//Constantes.VAR_PAYW_URL_BACK_W2
					resp.put("IDIOMA_RESPUESTA", "ES");
					resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt())); //UtilsService.getSMS(card.getCt()); 
					resp.put("ESTATUS_3D", "");
					
					if(resp.get("XID") != null){
						resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
						resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
					}
					
					LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
					
					return sendAuthorizationRequestPayworks(resp,idapp);
				}
				else{
					LOGGER.debug("[PAGO RECHAZADO]");
					
					mav = new ModelAndView("payworks/pagina-error");
					mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
					
					
					if( resp.get("XID") == null || ((String)resp.get("XID")).isEmpty()  || resp.get("CAVV") == null || ((String)resp.get("CAVV")).isEmpty()){
					
						mav.addObject("mensajeError", "El pago fue rechazado. " + 
								(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + "Su transacción no pudo ser procesada, porque su tarjeta no pudo ser autenticada bajo el programa 3dSecure" );
					}
					else
						mav.addObject("mensajeError", "El pago fue rechazado. " + 
							(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
				}

				
			}catch(Exception ex){
				LOGGER.error("ERROR AL PROCESAR RESPUESTA 3DS BANORTE "+ cadena, ex);
				LOGGER.error("ERROR ", ex);
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("idApp",idapp);
				mav.addObject("mensajeError", "Error inesperado, vuelva a intertarlo"  );

			}
			
			return mav;
		}
		
		private ModelAndView payDummy(int idApp, long idBitacora, Double amount, Double comision){
			ModelAndView mav = new ModelAndView("payworks/exito");
			SecureRandom sr = new SecureRandom();
			sr.nextBytes(new byte[1]);
			String referencia = sr.nextInt()+10000+"";
			String codAut = sr.nextInt()+10000+"";
			
			mav.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
					
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+DFormat.format(new Date())+"</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+referencia + "</td></tr>"
								
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+codAut + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+idBitacora + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+amount + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+comision + "</td></tr>"
								+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); 
								
			mav.addObject("idApp",idApp);
			return mav;
		}
		
		
public ModelAndView payworks2RecRespuesta(String NUMERO_CONTROL, String REFERENCIA, String FECHA_RSP_CTE, 
				String TEXTO, String RESULTADO_PAYW, String FECHA_REQ_CTE, String CODIGO_AUT, String CODIGO_PAYW, String RESULTADO_AUT, 
				String BANCO_EMISOR, String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA, ModelMap modelo, HttpServletRequest request){
			ModelAndView mav = new ModelAndView("payworks/exito");
			String msg;
			int idapp = 1;
			try{
				LOGGER.debug("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
						+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
						+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
						+" CODIGO_PAYW: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
						+" MARCA_TARJETA: " + MARCA_TARJETA);
				TransaccionBanorte TBanorte = new TransaccionBanorte();
				TBanorte.setBANCO_EMISOR(BANCO_EMISOR);
				TBanorte.setCODIGO_AUT(CODIGO_AUT);
				TBanorte.setCODIGO_PAYW(CODIGO_PAYW);
				TBanorte.setFECHA_REQ_CTE(FECHA_REQ_CTE);
				TBanorte.setFECHA_RSP_CTE(FECHA_RSP_CTE);
				TBanorte.setID_AFILIACION(ID_AFILIACION);
				TBanorte.setMARCA_TARJETA(MARCA_TARJETA);
				TBanorte.setNUMERO_CONTROL(NUMERO_CONTROL);
				TBanorte.setREFERENCIA(REFERENCIA);
				TBanorte.setRESULTADO_AUT(RESULTADO_AUT);
				TBanorte.setRESULTADO_PAYW(RESULTADO_PAYW);
				TBanorte.setTEXTO(TEXTO);
				TBanorte.setTIPO_TARJETA(TIPO_TARJETA);
				mapper.insertaTransactionBanorte(TBanorte);
				
				long id_bitacora =  Long.parseLong(NUMERO_CONTROL);
				TBitacoraBanorte bitacoraBanorte = mapper.getBitacoraBanorte(id_bitacora);
				
				idapp = bitacoraBanorte.getId_aplicacion();
				User user = mapper.getUserData(bitacoraBanorte.getId_usuario(), bitacoraBanorte.getId_aplicacion());
				String ReferenceBanorte = "0";
				String status3DS = bitacoraBanorte.getStatus3DS();
				
				if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
					
					PaymentRequest paymentRequest = new PaymentRequest();
					paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
					paymentRequest.setAmount(bitacoraBanorte.getAmount());
					paymentRequest.setConcept("Transferencia"); //TODO
					paymentRequest.setIdCard(bitacoraBanorte.getId_card());
					paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
					paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
					paymentRequest.setAuthProcom(CODIGO_AUT);
					paymentRequest.setRefProcom(REFERENCIA);
					mav = EnqueuePayment(paymentRequest,id_bitacora, bitacoraBanorte.getId_aplicacion());
					//mapper.updateBitacoraBanorte(id_bitacora, CODIGO_AUT+"-" + CODIGO_PAYW);
					//LOGGER.debug("Json Respuesta Mobilecard : " + mav.getModel().get("json"));
					MCTransafersResponse responseTransfer = gson.fromJson((String)mav.getModel().get("json"),MCTransafersResponse.class);
					
					if(responseTransfer.getIdError() == 0)
					{
						mav.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
							
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+DFormat.format(new Date())+"</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+REFERENCIA + "</td></tr>"
								
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+CODIGO_AUT + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+NUMERO_CONTROL + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+bitacoraBanorte.getAmount() + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+bitacoraBanorte.getComision() + "</td></tr>"
								+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); 
								
						mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
					//	mapper.updateBitacoraBanorte(Long.parseLong(responseTransfer.getReferenceBanorte()), id_bitacora, CODIGO_AUT+"-" + CODIGO_PAYW);
						HashMap< String, String> datos = new HashMap<String, String>();
						datos.put("NOMBRE", user.getUsr_nombre());
						datos.put("FECHA",DFormat.format(new Date()));
						datos.put("AUTBAN", CODIGO_AUT);
						datos.put("IMPORTE", bitacoraBanorte.getAmount()+"");
						datos.put("COMISION", bitacoraBanorte.getComision()+"");
						
						ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			    	      
						Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"), mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
						
					}else{
						msg = "Ocurrio un problema al realizar transferencia<br><br>" + responseTransfer.getMensajeError()+
								"<br>Para aclaraciones marque 01-800-925-5001.";
						mav = new ModelAndView("payworks/pagina-error");
						mav.addObject("mensajeError",msg);
						mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
					}
					
					
					
					status3DS = "200";
					
					
				}else
				{
					
					
					msg = 	("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
							"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
							"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
							+ ": " + TEXTO ;
					LOGGER.debug("[Mensaje Banorte] " + msg);
					
					msg = "El pago fue " + 
							("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
								"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
								"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
							" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO;
					mav = new ModelAndView("payworks/pagina-error");
					mav.addObject("mensajeError",msg);
					mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
				}
				
				
				TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				bitacora_banorte.setId_bitacora(id_bitacora);
				bitacora_banorte.setTexto(TEXTO);
				bitacora_banorte.setReferencia(REFERENCIA);
				bitacora_banorte.setResultado_payw(RESULTADO_PAYW);
				bitacora_banorte.setResultado_aut(RESULTADO_AUT);
				bitacora_banorte.setBanco_emisor(BANCO_EMISOR);
				bitacora_banorte.setTipo_tarjeta(TIPO_TARJETA);
				bitacora_banorte.setMarca_tarjeta(MARCA_TARJETA);
				bitacora_banorte.setCodigo_aut(CODIGO_AUT);
				bitacora_banorte.setBank_reference(ReferenceBanorte);
				bitacora_banorte.setTransaction_number(Long.parseLong(ReferenceBanorte));
				bitacora_banorte.setStatus3DS("200");
				
				mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
				
				
			}catch(Exception ex){
				LOGGER.error("[ERROR AL PROCESAR RESPUESTA BANORTE payworks2RecRespuesta] " + NUMERO_CONTROL, ex);
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("idApp",idapp);
				mav.addObject("mensajeError", 
						"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
						+ "Por favor vuelva a intentarlo en unos minutos.");
			}
			return mav;
		}

/*
 * Encola una transacción para transferencia
 * Se compara RFC,NUM CUENTA, COD BANCO, TIPO CUENTA para determinar que estén registradas
 */
public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora, int idApp){
	EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
	EnqueueTransactionResponse response = new EnqueueTransactionResponse();
	MCTransafersResponse responseTransfer = new MCTransafersResponse();
	ModelAndView view = new ModelAndView("payworks/exito");
	McBaseResponse McResponse = new McBaseResponse();
	try{
		login = login();
		TBitacoraVO bitacora = new TBitacoraVO();
		
		//SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
		responseTransfer.setComision(1);
		responseTransfer.setFecha(DFormat.format(new Date()));
		responseTransfer.setMontoTransfer(paymentRequest.getAmount());
		
		
		
		User user = mapper.getUserData(paymentRequest.getIdUser(), idApp);
		Card card = mapper.getCard(paymentRequest.getIdUser(),paymentRequest.getIdCard(), idApp);
		AccountRecord fromAccount = new AccountRecord();
		
		responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));
		
		fromAccount.setActNumber("0276948693"); //card.getNum_cuenta()
		fromAccount.setActType("ACT"); //card.getAct_type()
		fromAccount.setBankCode("072"); //card.getIdbanco()
		fromAccount.setContact("Jorge Silva Gallegos"); //card.getNombre_tarjeta()
		fromAccount.setEmail("jorge@mobilecardmx.com"); //user.geteMail()
		fromAccount.setHolderName("ADCEL SA DE CV"); //card.getNombre_tarjeta()
		fromAccount.setPhone("5555403124"); //user.getUsr_telefono()
		fromAccount.setRfc("ADC090715DW3");
		
		AccountRecord toAccount = mapper.getAccount(paymentRequest.getAccountId()); //new AccountRecord();
		
	//	LOGGER.debug("To Account: " + paymentRequest.getAccountId() + " " + gson.toJson(toAccount));
		//LOGGER.debug("From Account: " + fromAccount);
		
		
		
		payment.setAmount(paymentRequest.getAmount());
		payment.setClientReference(idBitacora+""); //"ACC000011"
		payment.setDescription(paymentRequest.getConcept());
		payment.setFromAccount(fromAccount);
		payment.setToAccount(toAccount);
		payment.setToken(login.getToken());
		response = h2hclient.EnqueuePayment(payment,paymentRequest.getIdioma());
		
		LOGGER.debug("[RESPUESTA PAGO] " + gson.toJson(response));
		
		if(response.getResult().isSuccess()){
			//String responseBD = mapper.H2HBanorte_Business(paymentRequest.getIdUser(), bitacora.getIdBitacora(), Long.parseLong(response.getTransactionReference()), "", 1, "es");
			LOGGER.debug("[Transaccion guardada] Exito");
			mapper.updateTbitacora(idBitacora,1,paymentRequest.getAuthProcom());
			HashMap< String, String> datos = new HashMap<String, String>();
			datos.put("NOMBRE", toAccount.getContact());
			datos.put("FECHA",DFormat.format(new Date()));
			
			datos.put("IMPORTE", payment.getAmount()+"");
			datos.put("CUENTA", toAccount.getActNumber());
			
			ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			
			Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_DEST_ES"), mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
		
			//mapper.updateBitacoraBanorte(Long.parseLong(response.getTransactionReference()), idBitacora, "");
			
		}
		else
		{
			LOGGER.debug("[Transaccion guardada] error al cobrar");
			mapper.updateTbitacora(idBitacora,2,"");
		}
		
		
		responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
		responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
		responseTransfer.setReferenceBanorte(response.getTransactionReference());
		responseTransfer.setRefProcom(paymentRequest.getRefProcom());
		
		McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
		McResponse.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
		//LOGGER.debug("JSON: " +  gson.toJson(responseTransfer));
		view.addObject("json", gson.toJson(responseTransfer));
		view.addObject("idApp",idApp);
		return view;//McResponse;
	}catch(Exception ex){
		//LOGGER.error("[ERROR AL ENCOLAR TRANSFERENCIA]  " +  gson.toJson(paymentRequest));
		LOGGER.error("[ERROR AL ENCOLAR PAGO] " , ex);
		responseTransfer.setIdError(99);
		responseTransfer.setMensajeError("Error al procesar Transaferencia");
		view.addObject("json", gson.toJson(responseTransfer));
		view.addObject("idApp",idApp);
		return view;//McResponse;
	}finally{
		logout();
	}
}


private ModelAndView SendAmex(User user,Card card, PaymentRequest request, int idApp, Proveedor proveedor, AfiliacionVO afiliacion, AccountRecord toAccount, double lat, double lon){
	
		ModelAndView mav = new ModelAndView("payworks/exito");
	try{
		
		double comision = request.getComision();
		BigDecimal bd = new BigDecimal(comision);
		bd = bd.setScale(2, RoundingMode.HALF_DOWN);
		comision = bd.doubleValue();
		double total = comision + request.getAmount();
		
		TBitacoraVO bitacora = new TBitacoraVO();
		bitacora.setTipo("");
		bitacora.setSoftware("");
		bitacora.setModelo("");
		bitacora.setWkey("");
		bitacora.setIdUsuario(request.getIdUser()+"");
		bitacora.setIdProducto(1);
		bitacora.setCar_id(1);
		bitacora.setCargo(request.getAmount()+comision);
		bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
		bitacora.setConcepto("Transferencia H2HBANORTE("+ (idApp == 1 ? "Mobilecard" : "Telecom") +" AMEX): Concepto: " + request.getConcept() + " Monto: "
				+ request.getAmount() + " Comision: " + comision );
		bitacora.setImei("");
		bitacora.setDestino( request.getAccountId()+"" );
		bitacora.setTarjeta_compra(card.getNumerotarjeta());
		bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
		bitacora.setComision(comision);
		bitacora.setCx("");
		bitacora.setCy("");
		bitacora.setPin("");
		bitacora.setId_aplicacion(idApp);
		mapper.addBitacora(bitacora);
		
		TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
		bitacora_banorte.setAccount_id(request.getAccountId());
		bitacora_banorte.setAmount(request.getAmount());
		bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
		bitacora_banorte.setId_card(request.getIdCard());
		bitacora_banorte.setId_usuario(request.getIdUser());
		bitacora_banorte.setIdioma(request.getIdioma());
		bitacora_banorte.setTransaction_number(0);
		bitacora_banorte.setBank_reference("");
		bitacora_banorte.setComision(comision);
		bitacora_banorte.setTexto("");
		bitacora_banorte.setReferencia("");
		bitacora_banorte.setResultado_payw("");
		bitacora_banorte.setResultado_aut("");
		bitacora_banorte.setBanco_emisor("");
		bitacora_banorte.setTipo_tarjeta("");
		bitacora_banorte.setMarca_tarjeta("");
		bitacora_banorte.setCodigo_aut("");
		bitacora_banorte.setStatus3DS("");
		bitacora_banorte.setConcepto(request.getConcept());
		bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
		bitacora_banorte.setNombre_destino(toAccount.getHolderName());
		bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
		bitacora_banorte.setBanco_destino(toAccount.getBankCode());
		bitacora_banorte.setId_aplicacion(idApp);
		bitacora_banorte.setLat(lat);
		bitacora_banorte.setLon(lon);
		//insertando en bitacora McBanorte
		mapper.insertTbitacoraMCBanorte(bitacora_banorte);
		
		
		
		AmexAutorization pago = new AmexAutorization();
		pago.setAfiliacion("9353192983");
		pago.setComision(comision);
		pago.setCvv2(UtilsService.getSMS(card.getCt()));
		pago.setDom_amex(card.getUsrDomAmex() == null ? "" : card.getUsrDomAmex());
		pago.setMonto(total);
		pago.setProducto("TransfersH2H");
		pago.setTarjeta(UtilsService.getSMS(card.getNumerotarjeta()));
		pago.setVigencia(UtilsService.getSMS(card.getVigencia()) );
		
		
		RespuestaAmex respAmex = amexService.solicitaAutorizacionAmex(pago);
		// respAmex.setCode("000"); //TODO QA
		if(respAmex.getCode().equals("000")){  
			
			PaymentRequest paymentRequest = new PaymentRequest();
			paymentRequest.setAccountId(request.getAccountId());
			paymentRequest.setAmount(request.getAmount());
			paymentRequest.setConcept("Transferencia"); //TODO
			paymentRequest.setIdCard(request.getIdCard());
			paymentRequest.setIdioma(request.getIdioma());
			paymentRequest.setIdUser(request.getIdUser());
			paymentRequest.setAuthProcom(respAmex.getTransaction());
			paymentRequest.setRefProcom(respAmex.getTransaction());
			mav = EnqueuePayment(paymentRequest,bitacora.getIdBitacora(), idApp);
			
			MCTransafersResponse responseTransfer = gson.fromJson((String)mav.getModel().get("json"),MCTransafersResponse.class);
			if(responseTransfer.getIdError() == 0)
			{
				mav.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
					
						+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+DFormat.format(new Date())+"</td></tr>"
						//+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+respAmex.getTransaction() + "</td></tr>"
						
						+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+respAmex.getTransaction() + "</td></tr>"
						+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+bitacora.getIdBitacora() + "</td></tr>"
						+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+request.getAmount() + "</td></tr>"
						+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+request.getComision() + "</td></tr>"
						+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); 
						
				mav.addObject("idApp",idApp);
			//	mapper.updateBitacoraBanorte(Long.parseLong(responseTransfer.getReferenceBanorte()), id_bitacora, CODIGO_AUT+"-" + CODIGO_PAYW);
				HashMap< String, String> datos = new HashMap<String, String>();
				datos.put("NOMBRE", user.getUsr_nombre());
				datos.put("FECHA",DFormat.format(new Date()));
				datos.put("AUTBAN", respAmex.getTransaction());
				datos.put("IMPORTE", request.getAmount()+"");
				datos.put("COMISION", comision+"");
				
				ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
	    	      
				Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"), mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
			}else{
				//Error en encolamiento de transferencia
				String msg = "Ocurrio un problema al realizar transferencia<br><br>" + responseTransfer.getMensajeError()+
						"<br>Para aclaraciones marque 01-800-925-5001.";
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError",msg);
				mav.addObject("idApp",idApp);
			}
			
			//actualizando bitacora banorte
			bitacora_banorte = new TBitacoraBanorte();
			bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			bitacora_banorte.setTexto("TRANSFERENCIA AMEX");
			bitacora_banorte.setReferencia(respAmex.getTransaction());
			bitacora_banorte.setResultado_payw("A");
			bitacora_banorte.setResultado_aut(respAmex.getTransaction());
			bitacora_banorte.setBanco_emisor("AMEX");
			bitacora_banorte.setTipo_tarjeta("AMEX");
			bitacora_banorte.setMarca_tarjeta("AMEX");
			bitacora_banorte.setCodigo_aut(respAmex.getTransaction());
			bitacora_banorte.setBank_reference(responseTransfer.getReferenceBanorte());
			bitacora_banorte.setTransaction_number(Long.parseLong(responseTransfer.getReferenceBanorte()));
			bitacora_banorte.setStatus3DS("200");
			mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
			
			
			
		}else{
			//pago denegado AMEX
			String msg = "El pago fue " +  "RECHAZADO" + " Descripcion: " + respAmex.getErrorDsc();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError",msg);
			mav.addObject("idApp",idApp);
		}
		
	}catch(Exception ex){
		LOGGER.error("[ERROR AL PROCESAR PAGO AMEX] "+ request.getIdUser(), ex);
		mav = new ModelAndView("payworks/pagina-error");
		mav.addObject("idApp",idApp);
		mav.addObject("mensajeError", 
				"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
				+ "Por favor vuelva a intentarlo en unos minutos.");
	}
	
	return mav;
}

public ModelAndView sendAuthorizationRequestPayworks(HashMap<String, Object> resp, Integer idApp) {
	
	RestTemplate restTemplate = new RestTemplate();
    ModelAndView mav = null;
    try {
    	
    	HttpHeaders headers = new HttpHeaders();
    	HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Content-Type","application/x-www-form-urlencoded");
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("ID_AFILIACION", (String) resp.get("ID_AFILIACION"));
        map.add("USUARIO", (String)resp.get("USUARIO"));
        map.add("CLAVE_USR", (String)resp.get("CLAVE_USR"));
        map.add("CMD_TRANS", "VENTA");
        map.add("ID_TERMINAL", (String)resp.get("ID_TERMINAL"));
        map.add("MONTO", (String)resp.get("Total"));
        map.add("MODO", "PRD");
        map.add("NUMERO_CONTROL", (String) resp.get("Reference3D"));
        map.add("NUMERO_TARJETA", (String) resp.get("Number"));
        map.add("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
        map.add("CODIGO_SEGURIDAD", (String) resp.get("CODIGO_SEGURIDAD"));
        map.add("MODO_ENTRADA", "MANUAL");
        map.add("URL_RESPUESTA", (String) resp.get("URL_RESPUESTA"));
        map.add("IDIOMA_RESPUESTA", "ES");
        map.add("ESTATUS_3D", "");
        map.add("ECI", "");
		if(resp.get("XID") != null && resp.get("CAVV") != null){
			map.add("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
            map.add("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
		}
					
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        LOGGER.debug("INFO SEND PAYWORKS: " + map.toString());
        
        HttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy() {
                    @Override
                    protected URI createLocationURI(String location) throws org.apache.http.ProtocolException {
                 	   String urlEncode = null;
                 	   urlEncode = location.replace(" ", "");
                 	 //  LOGGER.info("URL - {} - ENCODE URL - {}", location, urlEncode);
                        return super.createLocationURI(urlEncode);
                    }
                })
                .build();
         factory.setHttpClient(httpClient);
         restTemplate.setRequestFactory(factory);
        
        ResponseEntity<String> response = restTemplate.postForEntity("https://via.pagosbanorte.com/payw2", request, String.class);
        LOGGER.info("AUTORIZACION SOLICITADA A PAYWORKS ", response.getBody());
        TBitacoraBanorte bitacoraBanorte =   mapper.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
        
        if(bitacoraBanorte.getResultado_payw() != null && bitacoraBanorte.getResultado_payw().equals("A")){
        	mav = new ModelAndView("payworks/exito");
        	mav.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
					
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+DFormat.format(new Date())+"</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+bitacoraBanorte.getReferencia() + "</td></tr>"
								
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+bitacoraBanorte.getCodigo_aut() + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+bitacoraBanorte.getId_bitacora() + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+bitacoraBanorte.getAmount() + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+bitacoraBanorte.getComision() + "</td></tr>"
								+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); 
								
						mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
        }else{
        	
        	String msg = "El pago fue " + 
					("D".equalsIgnoreCase(bitacoraBanorte.getResultado_payw())? "DECLINADO ": 
						"R".equalsIgnoreCase(bitacoraBanorte.getResultado_payw())? "RECHAZADO ":
						"T".equalsIgnoreCase(bitacoraBanorte.getResultado_payw())? "Sin respuesta del autorizador":"Repuesta no Valida ") +bitacoraBanorte.getBanco_emisor()+", "+bitacoraBanorte.getTipo_tarjeta()+
					" . " + (bitacoraBanorte.getResultado_aut() != null? "Clave: " +bitacoraBanorte.getResultado_payw():bitacoraBanorte.getResultado_aut() != null? bitacoraBanorte.getResultado_aut(): "") + ", Descripcion: " + bitacoraBanorte.getTexto();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError",msg);
			mav.addObject("idApp",bitacoraBanorte.getId_aplicacion());
        }
        
        
        
    	
    }catch(Exception ex){
    	LOGGER.error("ERROR AL ENVIAR PETICION PAYWORKS REST: ", ex);
    	mav = new ModelAndView("payworks/pagina-error");
		mav.addObject("idApp",idApp);
		mav.addObject("mensajeError", "Error inesperado, vuelva a intertarlo"  );
    }
    
    return mav;
	
}



public LoginResponse login(){
	try{
		LoginRequest login  = new LoginRequest();
		login.setLogin("addcel_banorte");
		login.setPassword("B4N0rt3%");
		LOGGER.debug("iniciando login");
		LoginResponse response = h2hclient.login(login, "es");
		LOGGER.debug("LOGUIN: "+  response.getToken() + " " + response.getResult().isSuccess() + " " + response.getResult().getMessage());
		return response;
	}catch(Exception ex){
		LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
		return new  LoginResponse();
	}
}

public LogoutResponse logout(){
	try{
		LogoutRequest logout = new LogoutRequest();
		logout.setLogin("addcel_banorte");
		logout.setToken(login.getToken());
		LogoutResponse response = h2hclient.logout(logout, "es");
		LOGGER.debug("[RESPUESTA DE LOGOUT]" +  response.getResult().isSuccess());
		return response;
	}catch(Exception ex){
		LOGGER.error("[ERROR EN LOGOUT]", ex);
		return new LogoutResponse();
	}
}
	
}
