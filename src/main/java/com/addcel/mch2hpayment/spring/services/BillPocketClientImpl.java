package com.addcel.mch2hpayment.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.addcel.mch2hpayment.client.model.AutorizationManualRequest;
import com.addcel.mch2hpayment.client.model.BillPocketClient;
import com.addcel.mch2hpayment.client.model.BillPocketResponse;
import com.addcel.mch2hpayment.client.model.RefundReq;
import com.google.gson.Gson;

@Service
public class BillPocketClientImpl  implements BillPocketClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketClientImpl.class);
	
	private final static String PATH_AUTHORIZATION_MANUAL = "http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/authorization/manual";
	
	private final static String PATH_REFUND = "http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/refund";
	
	
	private Gson GSON = new Gson();
	
	
	@Override
	public BillPocketResponse AutorizationManual(AutorizationManualRequest requestManual,Integer idApp,Integer idPais, String idioma) {
		BillPocketResponse response = null;
		try{
			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<AutorizationManualRequest> request = new HttpEntity<>(requestManual,headers);
	        
	        ResponseEntity<String> respBP = restTemplate.exchange(PATH_AUTHORIZATION_MANUAL.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{idioma}", idioma),HttpMethod.POST, request, String.class);
	        LOGGER.info("RESPUESTA DE BILL POCKET - {}", respBP.getBody());
	        response = GSON.fromJson(respBP.getBody(), BillPocketResponse.class);
		}catch(Exception ex){
			LOGGER.error("ERROR AL MOMENTO DE ENVIAR PETICON BILLPOCKET", ex);
		}
		
		return response;
	}
	
	@Override
	public BillPocketResponse Refund(long idTransaccion, Integer idApp,Integer idPais, String idioma) {
		
		BillPocketResponse response = new BillPocketResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE REEMBOLSO {}" , idTransaccion);
			RestTemplate restTemplate = new RestTemplate();
			
			RefundReq req = new RefundReq();
			req.setIdTransaccion(idTransaccion);
			
			HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        
	        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_REFUND.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{idioma}", idioma))
	                .queryParam("idTransaccion", idTransaccion);
	        
	        
	        HttpEntity<RefundReq> request = new HttpEntity<>(headers);
	        
	        ResponseEntity<String> respBP = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.GET, request, String.class);
	        LOGGER.info("RESPUESTA DE BILL POCKET REFUND - {}", respBP.getBody());
	        response = GSON.fromJson(respBP.getBody(), BillPocketResponse.class);
	        
		}catch(Exception ex){
			LOGGER.error("ERROR AL REEMBOLSAR MONTO BILLPOCKET", ex);
			response.setCode(5000);
			if(idioma.equals("es"))
				response.setMessage("Error al procesar reembolso");
			else
				response.setMessage("Error processing refund");
		}
		
		return response;
	}

	
	
}
