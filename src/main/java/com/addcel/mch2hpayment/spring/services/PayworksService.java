package com.addcel.mch2hpayment.spring.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.ServiceMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.PayworksVO;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;
import com.google.gson.Gson;

@Service
public class PayworksService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksService.class);
	private String messageId;
	
	private Gson gson = new Gson();
	private SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");;
	private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	
	@Autowired
	ServiceMapper mapper;
	
	public ModelAndView send3DSBanorte(PaymentRequest request){
		messageId = request.getIdUser()+"";
		ModelAndView view = new ModelAndView();
		AfiliacionVO afiliacion = null;
		try{
			
			Calendar calendario = new GregorianCalendar();
			int hora =calendario.get(Calendar.HOUR_OF_DAY);
			//int minutos = calendario.get(Calendar.MINUTE);
			//int segundos = calendario.get(Calendar.SECOND);
			Card card = mapper.getCard(request.getIdUser(), request.getIdCard()); //
			
			if(request.getIdCard() != 374764)
			if( hora >= 22 || (hora >= 0 && hora <2)){
				LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				else
					view.addObject("mensajeError","Service available during the hours of 2:00 to 22:00.");
				
				return view;
			}
				
			
				
					
					String  rules_response = mapper.RulesH2HBanorte(request.getIdUser(), request.getAccountId(), request.getIdCard(),request.getAmount(), UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6),request.getIdioma());
					McBaseResponse Rules = gson.fromJson(rules_response, McBaseResponse.class); 
					if(Rules.getIdError() != 0){
						LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
						view = new ModelAndView("payworks/pagina-error");
						view.addObject("mensajeError", Rules.getMensajeError());
						return view;
					}
					
				
			
			
			
			User user = mapper.getUserData(request.getIdUser());
			if(user.getId_usr_status() != 1){
				LOGGER.info("[DETECTAMOS USUARIO INACTIVO] " + gson.toJson(request));
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Existen problemas su usuario Mobilecard, Para aclaraciones marque 01-800-925-5001.");
				else
					view.addObject("mensajeError","There are problems with your Mobilecard user. For clarifications call 01-800-925-5001.");
				
				return view;
			}
			
			AccountRecord toAccount = mapper.getAccount(request.getAccountId());
			
			if(toAccount.getBankCode().equals("072") || toAccount.getBankCode().equals("032")){
				
				if(toAccount.getActType().equals("CLABE") || toAccount.getActNumber().length() > 11){
					
					//LOGGER.info("[Detectamos Cuenta banorte inconsistente] " + gson.toJson(request));
					view = new ModelAndView("payworks/pagina-error");
					if(request.getIdioma().equals("es"))
						view.addObject("mensajeError", 
								"Hemos detectado inconsistencias en la cuenta destino, favor de verificarla");
					else
						view.addObject("mensajeError","We have detected inconsistencies in the destination account, please verify it");
					
					return view;
		                 
				}
			}
			
			
			// validar maximo transferencia
			/*int max_tranfers = Integer.parseInt(mapper.getParameter("@MAX_TRANFER_AMOUNT_H2H"));
			
			int amount_transfers_user = mapper.GetAmountUserTransfer(request.getIdUser());
			if(request.getAmount() > max_tranfers || amount_transfers_user > max_tranfers || (amount_transfers_user+request.getAmount()) > max_tranfers  ){
				LOGGER.info("[Se excede el maximo de monto permitido] " + gson.toJson(request));
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", 
							"estimado usuario, solo puede transferir un monto no mayor a $" + max_tranfers +".00");
				else
					view.addObject("mensajeError","Dear user, you can only transfer an amount no greater than $" + max_tranfers +".00");
				
				return view;
			}*/
			
			
			TBitacoraVO bitacora = new TBitacoraVO();
			
			
			
			//detectar bin nacional
			int bin = mapper.isBinValido(UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6));
			
			if(bin == 0){
				LOGGER.info("[SE DETECTO UNA TARJETA NO BIN VALIDO] " + request.getIdUser() + " ");
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Estimado usuario, las transferencia solo aplica para tarjetas Mexicanas.");
				else
					view.addObject("mensajeError","Dear User, the transfer only applies to Mexican cards.");
				
				return view;
			}
			
			//Detectar tarjeta AMEX
			if(card.getIdfranquicia() == 3){
				LOGGER.info("[SE DETECTO UNA TARJETA AMEX] " + request.getIdUser() + " " + request.getIdCard());
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "Estimado usuario, estamos trabajando para poder realizar transferencias con tarjetas American Express.");
				else
					view.addObject("mensajeError","Dear User, we are working to make transfers with American Express cards.");
				
				return view;
			}
			
			if(card.getEstado() == -1 ){
				LOGGER.info("[SE DETECTO UNA TARJETA BLOQUEADA] " + request.getIdUser() );
				view = new ModelAndView("payworks/pagina-error");
				if(request.getIdioma().equals("es"))
					view.addObject("mensajeError", "La tarjeta seleccionada se encuentra bloqueda para realizar movimientos. Para aclaraciones marque 01-800-925-5001.");
				else
					view.addObject("mensajeError","The selected card is locked to make movements. For clarifications call 01-800-925-5001.");
				
				return view;
			}
			
			
			
			BankCodes bank = mapper.getBankcodesByClave(toAccount.getBankCode());
			double comisionCalc = bank.getComision_fija() + ((Double.valueOf(request.getAmount())+bank.getComision_fija()) * bank.getComision_porcentaje());
			if(request.getComision() == 0)
				request.setComision(comisionCalc);
			/*
			account.getComisionFija() + ((Double.valueOf(monto) + account.getComisionFija())
					* account.getComisionPorcentaje());
					*/
			
			Proveedor proveedor = mapper.getProveedor("H2HBanorte");
			afiliacion = mapper.buscaAfiliacion("BANORTEH2H");
			String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			String vig = UtilsService.getSMS(card.getVigencia());
			String ct = UtilsService.getSMS(card.getCt()); 
			//String mes = vig.split("/")[0];
			//String anio = "20"+vig.split("/")[1];
			LOGGER.debug("Comision calculada: " + comisionCalc);
			LOGGER.debug("comision request: " + request.getComision());
			LOGGER.debug("amount " + request.getAmount());
			String cardType = card.getIdfranquicia() == 1? "VISA" : "MC";
			double comision = request.getComision();
			BigDecimal bd = new BigDecimal(comision);
			bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			comision = bd.doubleValue();
			
			
			bitacora.setTipo("");
			bitacora.setSoftware("");
			bitacora.setModelo("");
			bitacora.setWkey("");
			bitacora.setIdUsuario(request.getIdUser()+"");
			bitacora.setIdProducto(1);
			bitacora.setCar_id(1);
			bitacora.setCargo(request.getAmount()+comision);
			bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			bitacora.setConcepto("Transferencia H2HBANORTE: Concepto: " + request.getConcept() + " Monto: "
					+ request.getAmount() + " Comision: " + comision );
			bitacora.setImei("");
			bitacora.setDestino( request.getAccountId()+"" );
			bitacora.setTarjeta_compra(card.getNumerotarjeta());
			bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			bitacora.setComision(comision);
			bitacora.setCx("");
			bitacora.setCy("");
			bitacora.setPin("");
			mapper.addBitacora(bitacora);
			
			TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			bitacora_banorte.setAccount_id(request.getAccountId());
			bitacora_banorte.setAmount(request.getAmount());
			bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			bitacora_banorte.setId_card(request.getIdCard());
			bitacora_banorte.setId_usuario(request.getIdUser());
			bitacora_banorte.setIdioma(request.getIdioma());
			bitacora_banorte.setTransaction_number(0);
			bitacora_banorte.setBank_reference("");
			bitacora_banorte.setComision(comision);
			bitacora_banorte.setTexto("");
			bitacora_banorte.setReferencia("");
			bitacora_banorte.setResultado_payw("");
			bitacora_banorte.setResultado_aut("");
			bitacora_banorte.setBanco_emisor("");
			bitacora_banorte.setTipo_tarjeta("");
			bitacora_banorte.setMarca_tarjeta("");
			bitacora_banorte.setCodigo_aut("");
			bitacora_banorte.setStatus3DS("");
			bitacora_banorte.setConcepto(request.getConcept());
			bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			bitacora_banorte.setBanco_destino(toAccount.getBankCode());
			//insertando en bitacora McBanorte
			mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			
			double total = comision + request.getAmount();
			
			
			PayworksVO payVO = new PayworksVO();
			payVO.setCard(numcard);//  numcard 4152313189416436
			payVO.setCardType(cardType);//cardType VISA
			payVO.setExpires(vig);//vig "04/21"
			payVO.setForwardPath(afiliacion.getForwardPath().split(" ")[0]);//Constantes.URLBACK3DSPAYW
			payVO.setMerchantId(afiliacion.getMerchantId());
			payVO.setReference3D(bitacora.getIdBitacora()+"");
			payVO.setTotal(Business.formatoMontoPayworks(total+""));
			
			LOGGER.info("ENVIO DE INFO AL 3DS BANORTE"+"["+messageId+"]"+"["+bitacora.getIdBitacora()+"] " + gson.toJson(payVO));
			
			view.setViewName("payworks/comercio-send");
			view.addObject("payworks", payVO);
			
		}catch(Exception ex){
			//LOGGER.info("["+messageId+"] " + gson.toJson(request));
			LOGGER.error("[ERROR AL ENVIAR COBRO 3DS]"+"["+messageId+"]", ex);
			/*MCTransafersResponse transfersresponse = new MCTransafersResponse();
			transfersresponse.setIdError(98);
			if(request.getIdioma().equals("es")){
				transfersresponse.setMensajeError("Error al procesar Transferencia");
			}
			else{
				transfersresponse.setMensajeError("Transfer Proccess Error");
			}
			view.setViewName("transfers_response");
			view.addObject("json", gson.toJson(transfersresponse));*/
			view = new ModelAndView("payworks/pagina-error");
			if(request.getIdioma().equals("es"))
				view.addObject("mensajeError", 
						"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
								+ "Por favor vuelva a intentarlo en unos minutos.");
			else
				view.addObject("mensajeError","Sorry for the inconvenience, we detected an error when authorizing your transaction. "
                 + "Please try again in a few minutes.");
		}
		
		return view;
	}
	
	// respuesta 3ds banorte y envio de confirmacion a banorte
	public ModelAndView payworksRec3DRespuesta(String cadena, ModelMap modelo){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		ModelAndView mav = null;
		try{
			cadena = cadena.replace(" ", "+");
			LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				LOGGER.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				AfiliacionVO afiliacion = mapper.buscaAfiliacion("BANORTEH2H");
				LOGGER.debug("idBitacora "+ resp.get("Reference3D").toString());
				 TBitacoraBanorte bitacoraBanorte =   mapper.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
				Card card = mapper.getCard(bitacoraBanorte.getId_usuario(), bitacoraBanorte.getId_card());
				mav=new ModelAndView("payworks/comercioPAYW2");
				mav.addObject("payworks", resp);
				
				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				resp.put("USUARIO", afiliacion.getUsuario());
				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				resp.put("Total", Business.formatoMontoPayworks((String)resp.get("Total")));
				resp.put("MODO", "PRD");
				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				resp.put("NUMERO_TARJETA", resp.get("Number"));
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
				resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));// UtilsService.getSMS(card.getCt()); 
				resp.put("MODO_ENTRADA", "MANUAL");
				resp.put("URL_RESPUESTA", afiliacion.getForwardPath().split(" ")[1]);//Constantes.VAR_PAYW_URL_BACK_W2
				resp.put("IDIOMA_RESPUESTA", "ES");
				resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt())); //UtilsService.getSMS(card.getCt()); 
				//resp.put("ESTATUS_3D", "200");
				
				if(resp.get("XID") != null){
					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
				}
				
				LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());	
			}
			else{
				LOGGER.debug("[PAGO RECHAZADO]");
			
				TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				bitacora_banorte.setId_bitacora(Long.parseLong((String)resp.get("Reference3D")));
				bitacora_banorte.setTexto( resp.get("Status") != null ?  (String)Constantes.errorPayW.get(resp.get("Status")) : "" );
				bitacora_banorte.setReferencia("");
				bitacora_banorte.setResultado_payw("");
				bitacora_banorte.setResultado_aut("");
				bitacora_banorte.setBanco_emisor("");
				bitacora_banorte.setTipo_tarjeta("");
				bitacora_banorte.setMarca_tarjeta((String)resp.get("CardType"));
				bitacora_banorte.setCodigo_aut("");
				bitacora_banorte.setBank_reference("");
				bitacora_banorte.setTransaction_number(0);
				bitacora_banorte.setStatus3DS( resp.get("Status") != null ? (String)resp.get("Status") : "");
				
				mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", "El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
			}

			
		}catch(Exception ex){
			LOGGER.error("ERROR AL PROCESAR RESPUESTA 3DS BANORTE "+ cadena, ex);
			LOGGER.error("ERROR ", ex);
		}
		
		return mav;
	}
	
	public ModelAndView payworks2RecRespuesta(String NUMERO_CONTROL, String REFERENCIA, String FECHA_RSP_CTE, 
			String TEXTO, String RESULTADO_PAYW, String FECHA_REQ_CTE, String CODIGO_AUT, String CODIGO_PAYW, String RESULTADO_AUT, 
			String BANCO_EMISOR, String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA, ModelMap modelo, HttpServletRequest request){
		ModelAndView mav = new ModelAndView("payworks/exito");
		String msg;
		try{
			LOGGER.debug("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
					+" CODIGO_PAYW: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);
			long id_bitacora =  Long.parseLong(NUMERO_CONTROL);
			TBitacoraBanorte bitacoraBanorte = mapper.getBitacoraBanorte(id_bitacora);
			User user = mapper.getUserData(bitacoraBanorte.getId_usuario());
			//RESULTADO_PAYW = "A"; //TODO
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				
				PaymentRequest paymentRequest = new PaymentRequest();
				paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
				paymentRequest.setAmount(bitacoraBanorte.getAmount());
				paymentRequest.setConcept("Transferencia"); //TODO
				paymentRequest.setIdCard(bitacoraBanorte.getId_card());
				paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
				paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
				paymentRequest.setAuthProcom(CODIGO_AUT);
				paymentRequest.setRefProcom(REFERENCIA);
				mav = EnqueuePayment(paymentRequest,id_bitacora);
				//mapper.updateBitacoraBanorte(id_bitacora, CODIGO_AUT+"-" + CODIGO_PAYW);
				//LOGGER.debug("Json Respuesta Mobilecard : " + mav.getModel().get("json"));
				MCTransafersResponse responseTransfer = gson.fromJson((String)mav.getModel().get("json"),MCTransafersResponse.class);
				
				if(responseTransfer.getIdError() == 0)
				{
					mav.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
							//+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
							+"<tr><td style='text-align:right; width:60%;'>Fecha:</td><td style='width:40%;'> "+DFormat.format(new Date())+"</td></tr>"
							+"<tr><td style='text-align:right; width:60%;'>Referencia:</td><td style='width:40%;'> "+REFERENCIA + "</td></tr>"
							//+"<br>Folio: "+RESULTADO_PAYW
							+"<tr><td style='text-align:right; width:60%;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='width:40%;'> "+CODIGO_AUT + "</td></tr>"
							+"<tr><td style='text-align:right; width:60%;'>Folio MC:</td><td style='width:40%;'> "+NUMERO_CONTROL + "</td></tr>"
							+"<tr><td style='text-align:right; width:60%;'>Importe Transferido:</td><td style='width:40%;'> $"+bitacoraBanorte.getAmount() + "</td></tr>"
							+"<tr><td style='text-align:right; width:60%;'>Comision:</td><td style='width:40%;'> $"+bitacoraBanorte.getComision() + "</td></tr>"
							//+"<br>Total (incluida comision): "+ 1
							+"<tr><td colspan='2'><BR>Servicio operado por Plataforma MC." + "</td></tr>"
							//+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
							+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); // Favor de guardar este comprobante de pago para "
							//+ "posibles aclaraciones.");
					
				//	mapper.updateBitacoraBanorte(Long.parseLong(responseTransfer.getReferenceBanorte()), id_bitacora, CODIGO_AUT+"-" + CODIGO_PAYW);
					HashMap< String, String> datos = new HashMap<String, String>();
					datos.put("NOMBRE", user.getUsr_nombre());
					datos.put("FECHA",DFormat.format(new Date()));
					datos.put("AUTBAN", CODIGO_AUT);
					datos.put("IMPORTE", bitacoraBanorte.getAmount()+"");
					datos.put("COMISION", bitacoraBanorte.getComision()+"");
					
					ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
		    	      
					Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"), mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
					
				}else{
					msg = "Ocurrio un problema al realizar transferencia<br><br>" + responseTransfer.getMensajeError()+
							"<br>Para aclaraciones marque 01-800-925-5001.";
					mav = new ModelAndView("payworks/pagina-error");
					mav.addObject("mensajeError",msg);
				}
				
				
				TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				bitacora_banorte.setId_bitacora(id_bitacora);
				bitacora_banorte.setTexto(TEXTO);
				bitacora_banorte.setReferencia(REFERENCIA);
				bitacora_banorte.setResultado_payw(RESULTADO_PAYW);
				bitacora_banorte.setResultado_aut(RESULTADO_AUT);
				bitacora_banorte.setBanco_emisor(BANCO_EMISOR);
				bitacora_banorte.setTipo_tarjeta(TIPO_TARJETA);
				bitacora_banorte.setMarca_tarjeta(MARCA_TARJETA);
				bitacora_banorte.setCodigo_aut(CODIGO_AUT);
				bitacora_banorte.setBank_reference(responseTransfer.getReferenceBanorte());
				bitacora_banorte.setTransaction_number(Long.parseLong(responseTransfer.getReferenceBanorte()));
				bitacora_banorte.setStatus3DS("200");
				
				mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
				
				
			}else
			{
				msg = 	("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				LOGGER.debug("[Mensaje Banorte] " + msg);
				/*MCTransafersResponse responseTransfer = new MCTransafersResponse();
				responseTransfer.setIdError(99);
				responseTransfer.setMensajeError("msg");
				responseTransfer.setTarjeta("");
				responseTransfer.setComision(0);
				
				responseTransfer.setFecha(DFormat.format(new Date()));
				responseTransfer.setMontoTransfer(bitacoraBanorte.getAmount());
				responseTransfer.setReferenceBanorte("");
				
				mav.addObject("json", gson.toJson(responseTransfer));*/
				msg = "El pago fue " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
							"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
							"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
						" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO;
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError",msg);
			}
			
			
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL PROCESAR RESPUESTA BANORTE payworks2RecRespuesta] " + NUMERO_CONTROL, ex);
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
		}
		return mav;
	}
	
	/*
	 * Encola una transacción para transferencia
	 * Se compara RFC,NUM CUENTA, COD BANCO, TIPO CUENTA para determinar que estén registradas
	 */
	public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora){
		EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		MCTransafersResponse responseTransfer = new MCTransafersResponse();
		ModelAndView view = new ModelAndView("payworks/exito");
		McBaseResponse McResponse = new McBaseResponse();
		try{
			login = login();
			TBitacoraVO bitacora = new TBitacoraVO();
			
			//SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			responseTransfer.setComision(1);
			responseTransfer.setFecha(DFormat.format(new Date()));
			responseTransfer.setMontoTransfer(paymentRequest.getAmount());
			
			
			
			User user = mapper.getUserData(paymentRequest.getIdUser());
			Card card = mapper.getCard(paymentRequest.getIdUser(),paymentRequest.getIdCard());
			AccountRecord fromAccount = new AccountRecord();
			
			responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));
			
			fromAccount.setActNumber("0276948693"); //card.getNum_cuenta()
			fromAccount.setActType("ACT"); //card.getAct_type()
			fromAccount.setBankCode("072"); //card.getIdbanco()
			fromAccount.setContact("Jorge Silva Gallegos"); //card.getNombre_tarjeta()
			fromAccount.setEmail("jorge@mobilecardmx.com"); //user.geteMail()
			fromAccount.setHolderName("ADCEL SA DE CV"); //card.getNombre_tarjeta()
			fromAccount.setPhone("5555403124"); //user.getUsr_telefono()
			fromAccount.setRfc("ADC090715DW3");
			
			AccountRecord toAccount = mapper.getAccount(paymentRequest.getAccountId()); //new AccountRecord();
			
		//	LOGGER.debug("To Account: " + paymentRequest.getAccountId() + " " + gson.toJson(toAccount));
			//LOGGER.debug("From Account: " + fromAccount);
			
			
			
			payment.setAmount(paymentRequest.getAmount());
			payment.setClientReference(idBitacora+""); //"ACC000011"
			payment.setDescription(paymentRequest.getConcept());
			payment.setFromAccount(fromAccount);
			payment.setToAccount(toAccount);
			payment.setToken(login.getToken());
			response = h2hclient.EnqueuePayment(payment,paymentRequest.getIdioma());
			
		//	LOGGER.debug("[RESPUESTA PAGO] " + gson.toJson(response));
			
			if(response.getResult().isSuccess()){
				//String responseBD = mapper.H2HBanorte_Business(paymentRequest.getIdUser(), bitacora.getIdBitacora(), Long.parseLong(response.getTransactionReference()), "", 1, "es");
				LOGGER.debug("[Transaccion guardada] Exito");
				mapper.updateTbitacora(idBitacora,1,paymentRequest.getAuthProcom());
				HashMap< String, String> datos = new HashMap<String, String>();
				datos.put("NOMBRE", toAccount.getContact());
				datos.put("FECHA",DFormat.format(new Date()));
				
				datos.put("IMPORTE", payment.getAmount()+"");
				datos.put("CUENTA", toAccount.getActNumber());
				
				ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
				
				Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_DEST_ES"), mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
			
				//mapper.updateBitacoraBanorte(Long.parseLong(response.getTransactionReference()), idBitacora, "");
				
			}
			else
			{
				LOGGER.debug("[Transaccion guardada] error al cobrar");
				mapper.updateTbitacora(idBitacora,2,"");
			}
			
			/*SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			responseTransfer.setComision(1);
			responseTransfer.setFecha(DFormat.format(new Date()));
			responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			responseTransfer.setMontoTransfer(paymentRequest.getAmount());
			responseTransfer.setReferenceBanorte(response.getTransactionReference());
			responseTransfer.setTarjeta(fromAccount.getActNumber());*/
			responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			responseTransfer.setReferenceBanorte(response.getTransactionReference());
			responseTransfer.setRefProcom(paymentRequest.getRefProcom());
			
			McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
			McResponse.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			//LOGGER.debug("JSON: " +  gson.toJson(responseTransfer));
			view.addObject("json", gson.toJson(responseTransfer));
			return view;//McResponse;
		}catch(Exception ex){
			//LOGGER.error("[ERROR AL ENCOLAR TRANSFERENCIA]  " +  gson.toJson(paymentRequest));
			LOGGER.error("[ERROR AL ENCOLAR PAGO] " , ex);
			responseTransfer.setIdError(99);
			responseTransfer.setMensajeError("Error al procesar Transaferencia");
			view.addObject("json", gson.toJson(responseTransfer));
			return view;//McResponse;
		}finally{
			logout();
		}
	}
	
	public LoginResponse login(){
		try{
			LoginRequest login  = new LoginRequest();
			login.setLogin("addcel_banorte");
			login.setPassword("B4N0rt3%");
			LOGGER.debug("iniciando login");
			LoginResponse response = h2hclient.login(login, "es");
			LOGGER.debug("LOGUIN: "+  response.getToken() + " " + response.getResult().isSuccess() + " " + response.getResult().getMessage());
			return response;
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			return new  LoginResponse();
		}
	}
	
	public LogoutResponse logout(){
		try{
			LogoutRequest logout = new LogoutRequest();
			logout.setLogin("addcel_banorte");
			logout.setToken(login.getToken());
			LogoutResponse response = h2hclient.logout(logout, "es");
			LOGGER.debug("[RESPUESTA DE LOGOUT]" +  response.getResult().isSuccess());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR EN LOGOUT]", ex);
			return new LogoutResponse();
		}
	}
	
}
