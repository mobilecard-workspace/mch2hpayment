package com.addcel.mch2hpayment.spring.services;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mch2hpayment.spring.model.ProcomVO;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;


public class ProcomBusiness {

	private static final Logger logger = LoggerFactory.getLogger(ProcomBusiness.class);
			
	/*public ProcomVO comercioFin(long idBitacora, double monto, String merchant) {
		String varTotal = formatoMontoProsa(Double.toString(monto));
		String referencia = Long.toString(idBitacora);
		
		String digest = Business.digest(
				new StringBuilder(merchant).append(Constantes.STORE).append(Constantes.TERM)
						.append(varTotal).append(Constantes.CURRENCY).append(referencia).toString());
		
		logger.info("Digest DiestelService: {}", digest);

		ProcomVO procomObj = new ProcomVO(varTotal, Constantes.CURRENCY, Constantes.ADDRESS, referencia,
				merchant , Constantes.STORE, Constantes.TERM, digest, Constantes.URLBACK);
		
		logger.debug("Referencia : {}", referencia);
		

		return procomObj;
	}*/
	
	
	
	
	private String formatoMontoProsa(String monto) {
		String varTotal = "000";
		String pesos = null;
		String centavos = null;
		if (monto.contains(".")) {
			pesos = monto.substring(0, monto.indexOf("."));
			centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			if (centavos.length() < 2) {
				centavos = centavos.concat("0");
			} else {
				centavos = centavos.substring(0, 2);
			}
			varTotal = pesos + centavos;
		} else {
			varTotal = monto.concat("00");
		}
		logger.info("Monto a cobrar 3dSecure: " + varTotal);
		return varTotal;
	}
	
}
