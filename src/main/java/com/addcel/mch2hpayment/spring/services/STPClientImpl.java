package com.addcel.mch2hpayment.spring.services;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mch2hpayment.client.model.MoneySendReq;
import com.addcel.mch2hpayment.client.model.MoneySendRes;
import com.addcel.mch2hpayment.client.model.STPClient;
import com.google.gson.Gson;

@Service
public class STPClientImpl implements STPClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(STPClientImpl.class);
	
	private final static String PATH_MONEY_SEND = "http://localhost:80/STPConsumer/api/{idApp}/{idPais}/{idioma}/enviarDinero";
	
	private Gson GSON = new Gson();
	
	@Override
	public MoneySendRes moneySend(MoneySendReq req, int idApp, int idPais,String idioma) {
		// TODO Auto-generated method stub
		MoneySendRes response = new MoneySendRes();
		RestTemplate restTemplate = new RestTemplate();;
		try{
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String credentials = "mobilecardmx:82007eb4238205c75ebcdefc06a01311";
			headers.add("Authorization", "Basic " + new String(Base64.getEncoder().encode(credentials.getBytes())));
			
			LOGGER.debug("ENVIANDO A STP: " + GSON.toJson(req));
			HttpEntity<MoneySendReq> request = new HttpEntity<MoneySendReq>(req,headers);
			response =  (MoneySendRes) restTemplate.postForObject(PATH_MONEY_SEND.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{idioma}", idioma) , request, MoneySendRes.class);
			LOGGER.debug("respuesta STP: " + GSON.toJson(response));
		}catch(Exception ex){
			LOGGER.debug("ERROR AL ENVIAR DINERO A TRAVES DE STP ", ex);
			response.setCode(2000);
			response.setMessage("Error inesperado, intente mas tarde");
		}
		
		return response;
	}
	
}
