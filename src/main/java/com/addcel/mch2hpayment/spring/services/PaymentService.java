package com.addcel.mch2hpayment.spring.services;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.AddMoneyReq;
import com.addcel.mch2hpayment.client.model.AddMoneyRes;
import com.addcel.mch2hpayment.client.model.AutorizationManualRequest;
import com.addcel.mch2hpayment.client.model.BillPocketClient;
import com.addcel.mch2hpayment.client.model.BillPocketResponse;
import com.addcel.mch2hpayment.client.model.MoneySendReq;
import com.addcel.mch2hpayment.client.model.MoneySendRes;
import com.addcel.mch2hpayment.client.model.PrevivaleClient;
import com.addcel.mch2hpayment.client.model.STPClient;
import com.addcel.mch2hpayment.client.model.ThreatMetrixClient;
import com.addcel.mch2hpayment.client.model.ThreatMetrixRequest;
import com.addcel.mch2hpayment.client.model.ThreatMetrixResponse;
import com.addcel.mch2hpayment.client.model.TokenClientMC;
import com.addcel.mch2hpayment.client.model.ValidateTokenRequest;
import com.addcel.mch2hpayment.client.model.ValidateTokenResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BillPocketAuthorization;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.BPPaymentRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.Token;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Utils;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PaymentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);
	
	private String messageId;
	
	private Gson GSON = new Gson();
	
	private SimpleDateFormat SDF = new SimpleDateFormat("ddhhmmssSSS");
	
	@Autowired
	H2HMapper mapper;
	
	@Autowired
	AmexService amexService;
	
	@Autowired
	ThreatMetrixClient tmClient;
	
	@Autowired
	BillPocketClient  billClient;
	
	@Autowired
	TokenClientMC tokenClient;
	
	@Autowired
	STPClient stpClient;
	
	@Autowired
	PrevivaleClient previvaleClient;

	public Token getToken(String data, String profileId, String sessionId, String remoteAddr, Integer idApp) {
		Token respuesta = new Token();
		String token;
		String json = null; 
		PaymentRequest pago = null;
		ThreatMetrixResponse validate = null;
		try {
			if(sessionId != null) {
				//LOGGER.debug("Cif:-----> " + Utils.encryptJson("{\"idUser\":111,\"idCard\":117,\"accountId\":12,\"amount\":203.00,\"concept\":\"concepto\",\"idioma\":\"es\",\"comision\":1.20}"));
				//LOGGER.debug("CIF Sensitive: " + AddcelCrypto.encryptSensitive(SDF.format(new Date()), "{\"idUser\":111,\"idCard\":117,\"accountId\":12,\"amount\":203.00,\"concept\":\"concepto\",\"idioma\":\"es\",\"comision\":1.20}"));
				LOGGER.info("JSON: {}", data);
				json = Utils.decryptJson(data);
				LOGGER.info("JSON: {}", json);
				pago = GSON.fromJson(json, PaymentRequest.class);
				validate = validatePayment(pago, sessionId,idApp);
				if("high".equals(validate.getTmxRisk())) {
					respuesta.setMensajeError("Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
					respuesta.setIdError(900);
			} else {
						if("neutral".equals(validate.getTmxRisk())) {
							respuesta.setSecure(true);
						}
						token = mapper.getToken();
						LOGGER.info("TOKEN BD: "+token);
						token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
						LOGGER.info("TOKEN BD: "+token);
						token = token + "#" + pago.getIdUser() + "#" + pago.getIdCard() + "#" 
								+ sessionId + "#" + remoteAddr + "#" + pago.getAmount() + "#" + pago.getComision();
						LOGGER.info("TOKEN GENERADO: "+token);
						token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
						LOGGER.info("TOKEN GENERADO: "+token);
						respuesta.setAccountId(Utils.digest(String.valueOf(pago.getIdUser())));
						respuesta.setToken(token);
						respuesta.setMensajeError("Success");
						json = GSON.toJson(respuesta);
						json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
				}
			} else {
				LOGGER.error("ERROR - LA PETICION NO INCLUYE SESSION ID TMX");
				respuesta.setMensajeError("Peticion incompleta.");
				respuesta.setIdError(400);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
			respuesta.setMensajeError("No fue posible consultar el token en BD");
			respuesta.setIdError(-1);
		}
		LOGGER.info("CONSULTA DE TOKEN FINALIZADA");
		return respuesta;
	}

	public boolean validaToken(String header, String remoteAddr, String id, BPPaymentRequest data, String accountId,int idApp, int idPais,String idioma) {
		// TODO Auto-generated method stub
		String headerArray[] = null;
		String token = null;
		String ip = null;
		int tokenBd = 0;
		long idUsuario;
		long idTarjeta;
		Double amount = null;
		Double comision = null;
		
		try{
			LOGGER.debug("HEADER: {} - IP - {}", header, remoteAddr + " SESSION ID: " + id);
		
			headerArray = header.split("#");
			token = header;//headerArray[0];
			//token = AddcelCrypto.decryptSensitive(token);
			LOGGER.debug("token descifrado: " + AddcelCrypto.decryptSensitive(token));
			
			ValidateTokenRequest validate = new ValidateTokenRequest();
			validate.setAccountId(accountId);
			validate.setIdUsuario(data.getIdUser());
			validate.setJson(AddcelCrypto.encryptSensitive(SDF.format(new Date()), GSON.toJson(data)));
			validate.setToken(token);
			
			LOGGER.debug("VALIDANDO TOKEN... " + GSON.toJson(validate));
			ValidateTokenResponse resToken =  tokenClient.validateToken(validate, idApp, idPais, idioma,token);
			 
			if(resToken != null && resToken.getCode() == 0){
				LOGGER.debug("TOKEN VALIDO...");
				return true;
			}
		
		}catch(Exception e){
			LOGGER.error("ERROR AL VALIDAR TOKEN: ", e);	
		}
		
		return false;
	}

	public BPPaymentResponse processPayment(Integer idApp, Integer idPais, String idioma, BPPaymentRequest data) {
		BPPaymentResponse response = null;
		BillPocketResponse responseBP = null;
		TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
		String devolucion = "";
		try {
			LOGGER.debug("INCIANDO TRANSFERENCIA " + GSON.toJson(data));
			
			Proveedor proveedor = mapper.getProveedor("H2HBanorte");
			Card card = mapper.getCard(data.getIdUser(), data.getIdCard(), idApp);
			AccountRecord account = mapper.getAccount(data.getAccountId());
			String  rules_response = mapper.RulesH2HBanorte(data.getIdUser(), data.getAccountId(), data.getIdCard(),data.getAmount(), UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6),idioma, idApp);
			McBaseResponse Rules = GSON.fromJson(rules_response, McBaseResponse.class); 
			
			
			bitacora_banorte.setAccount_id(data.getAccountId());
			bitacora_banorte.setAmount(data.getAmount());
			bitacora_banorte.setId_card(data.getIdCard());
			bitacora_banorte.setId_usuario(data.getIdUser());
			bitacora_banorte.setIdioma(idioma);
			bitacora_banorte.setComision(data.getComision());
			bitacora_banorte.setConcepto("Transferencia Electronica MobileCard a " + account.getHolderName());
			bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			bitacora_banorte.setId_aplicacion(idApp);
			bitacora_banorte.setLat(data.getLat());
			bitacora_banorte.setLon(data.getLon());
			
			bitacora_banorte.setNombre_destino(account.getHolderName());
			bitacora_banorte.setCuenta_clabe_destino(account.getActNumber());
			bitacora_banorte.setBanco_destino(account.getBankCode());
			bitacora_banorte.setId_bitacora(0l);
			bitacora_banorte.setBank_reference("0"); 
			bitacora_banorte.setStatus3DS("-1");
			bitacora_banorte.setResultado_payw("");
			bitacora_banorte.setResultado_aut("");
			bitacora_banorte.setCodigo_aut("");
			bitacora_banorte.setTexto("");
			
			
			if(Rules.getIdError() != 0){
				LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
				response = new BPPaymentResponse();
				response.setMessage(Rules.getMensajeError());
				response.setCode(Rules.getIdError());
				response.setMaskedPAN(Business.maskCard(Business.getSMS(card.getNumerotarjeta()), "*"));
				return response;
			}
			
			Bank ba = mapper.getCatalogoBancosByClave(account.getBankCode());
			if(ba == null){
				response = new BPPaymentResponse();
				if(idioma.equals("es"))
					response.setMessage("Por cambio en las políticas, únicamente se aceptarán cuentas clabes, por favor actualiza tus beneficiarios");
				else
					response.setMessage("For change in policies, only clabe accounts will be accepted, please update your beneficiaries");
				response.setCode(800);
				response.setMaskedPAN(Business.maskCard(Business.getSMS(card.getNumerotarjeta()), "*"));
				return response;
			}
			
			
			responseBP = authorization(idApp, idPais, idioma, data, proveedor, account);//sendAutorizationManual(idApp, idPais, idioma, data);
			response = new BPPaymentResponse();
			response.setAmount(responseBP.getAmount()+data.getComision());
			response.setAuthNumber(responseBP.getAuthNumber());
			response.setDateTime(responseBP.getDateTime());
			response.setIdTransaccion(responseBP.getIdTransaccion());
			response.setMaskedPAN(responseBP.getMaskedPAN());
			response.setOpId(responseBP.getOpId());
			response.setStatus(responseBP.getStatus());
			response.setTicketUrl(responseBP.getTicketUrl());
			response.setTxnISOCode(responseBP.getTxnISOCode());
			response.setMessage(responseBP.getMessage());
			response.setCode(responseBP.getCode());
			bitacora_banorte.setId_bitacora(responseBP.getIdTransaccion());
			bitacora_banorte.setCodigo_aut(responseBP.getAuthNumber());
			bitacora_banorte.setStatus3DS(responseBP.getCode()+"");
			bitacora_banorte.setTexto(bitacora_banorte.getTexto()+"Response BillPocket:" +responseBP.getCode()+":"+responseBP.getMessage());
			
			
		if(responseBP.getCode() == 0){
			//COBRO EXITOSO
			MoneySendReq request = new MoneySendReq();
			request.setConceptoPago("Transferencia Electronica MobileCard a " + account.getHolderName());//data.getConcept()
			request.setCuentaBeneficiario(account.getActNumber());
			request.setIdUsuario(data.getIdUser());
			request.setInstitucionContraparte(account.getBankCode()); //Verificar que en alta comercio se gaurde esta varible en la columna del catalogo que paso vic
			DecimalFormat formato1 = new DecimalFormat("#.00");
			
			request.setMonto(formato1.format(data.getAmount()));
			request.setNombreBeneficiario(account.getHolderName());
			request.setTipoCuentaBeneficiario("40");//CLABE
			LOGGER.debug("ENVIANDO PRIMER INTENTO..." );
			/*********************PRIMER INTENTO*************************************/
			MoneySendRes respSTP =  stpClient.moneySend(request, idApp, idPais, idioma);
			
			if(respSTP.getMessage().equals("Error validando la firma")){
				/*******************************SEGUNDO INTENTO**********************/
				LOGGER.debug("SEGUNDO INTENTO STP ...");
				respSTP =  stpClient.moneySend(request, idApp, idPais, idioma);
			}
			
			if(respSTP.getCode() != 1000){
				LOGGER.debug("NO SE PUDO TRANSFERIR DINERO CON STP: {} {}",respSTP.getCode()+":"+ data.getIdUser() , respSTP.getMessage() );
				LOGGER.debug("PREVIVALE: " + card.getPrevivale());
				if(card.getPrevivale() != null &&  card.getPrevivale() == 1){
					LOGGER.debug("INICIANDO DEVOLUCION A TARJETA PREVIVALE :" + responseBP.getAmount() + ":" + responseBP.getIdTransaccion()+":"+data.getIdUser());
					AddMoneyReq addMoney = new AddMoneyReq();
					addMoney.setCantidad(responseBP.getAmount());
					addMoney.setIdUsuario(data.getIdUser());
					addMoney.setTarjeta(card.getNumerotarjeta());
					AddMoneyRes respPre = previvaleClient.agregarDinero(addMoney, idApp, idPais, idioma);
					
					response.setCode(6000);
					if(idioma.equals("es"))
						response.setMessage("No se pudo realizar la transferencia"+ respSTP.getMessage() +" Respuesta devolucion: " + respPre.getMessage());
					else
						response.setMessage("The transfer could not be made"+ respSTP.getMessage() +" Refund response: " + respPre.getMessage());

					
					if(respPre.getCode() == 1000)
						devolucion = ":Devolucion Aprobada:"+respPre.getData().getIdPeticion();
					else
						devolucion = ":Error devolucion";
					
					LOGGER.debug("FINALIZANDO DEVOLUCION A TARJETA PREVIVALE");
				}else{
					
					/************************INICIANDO DEVOLUCION CON BILLPOCKET********************************/
					BillPocketResponse  respRefund = billClient.Refund(responseBP.getIdTransaccion(), idApp, idPais, idioma);
					
					response.setCode(6000);
					if(idioma.equals("es"))
						response.setMessage("No se pudo realizar la transferencia"+ respSTP.getMessage() +" Respuesta devolucion: " + respRefund.getMessage());
					else
						response.setMessage("The transfer could not be made"+ respSTP.getMessage() +" Refund response: " + respRefund.getMessage());

									
				}
			}
			
			bitacora_banorte.setBank_reference(respSTP.getIdOperacion()+""); 
			bitacora_banorte.setResultado_payw(respSTP.getIdPeticionWsStp());
			bitacora_banorte.setResultado_aut(respSTP.getCode()+":" + (respSTP.getReferencia() != null ? respSTP.getReferencia() : "") );
			bitacora_banorte.setTexto(bitacora_banorte.getTexto() + "*Respuesta STP:"+respSTP.getCode()+":"+respSTP.getMessage()+devolucion);
			
		}
			
		} catch (Exception e) {
			LOGGER.error("error al procesar transferencia", e);
			response = new BPPaymentResponse();
			response.setCode(600);
			if(idioma.equals("es"))
				response.setMessage("ERROR AL PROCESAR TRANSFERENCIA, CONTACTE A SOPORTE");
			else
				response.setMessage("ERROR WHEN PROCESSING TRANSFER, CONTACT SUPPORT");
			e.printStackTrace();
		}finally{
			LOGGER.debug("insetando bitacora");
			insertaBitacoraBanorte(bitacora_banorte);
		}
		
		LOGGER.debug("RETORNANDO RESPUESTA: " + GSON.toJson(response));
		return response;
	}

	public BillPocketResponse authorization(Integer idApp, Integer idPais, String idioma, BPPaymentRequest data,Proveedor proveedor,AccountRecord accountDes) {
		BillPocketAuthorization authorization = new BillPocketAuthorization();
		BillPocketResponse response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			authorization.setIdUsuario(Long.valueOf(data.getIdUser()));
			authorization.setIdProveedor(proveedor.getId_proveedor().intValue());
			authorization.setIdProducto(1);
			authorization.setConcepto("Transferencia Electronica MobileCard a " + accountDes.getHolderName()); //data.getConcept()
			authorization.setImei(data.getImei());
			authorization.setSoftware(data.getSoftware());
			authorization.setModelo(data.getModel());
			authorization.setLat(data.getLat());
			authorization.setLon(data.getLon());
			authorization.setIdTarjeta(data.getIdCard());
			authorization.setCargo(data.getAmount());
			authorization.setComision(data.getComision());
			authorization.setReferencia(data.getReferencia());
			authorization.setEmisor("");
			authorization.setOperacion("");
			HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<BillPocketAuthorization> request = new HttpEntity<>(authorization, headers);
	        LOGGER.debug("ENVIANDO A BILLPOCKET: " + GSON.toJson(authorization));
	        ResponseEntity<String> respBP = restTemplate.exchange("http://localhost/BillPocket/"+idApp+"/"+idPais+"/"+idioma+"/authorization",
	                HttpMethod.POST, request, String.class);
	        LOGGER.info("RESPUESTA DE BILL POCKET - {}", respBP.getBody());
	        response = GSON.fromJson(respBP.getBody(), BillPocketResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	private void insertaBitacoraBanorte(TBitacoraBanorte bitacora){
		try{
			if(bitacora.getId_bitacora() > 0){
				TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				bitacora_banorte.setAccount_id(bitacora.getAccount_id());
				bitacora_banorte.setAmount(bitacora.getAmount());
				bitacora_banorte.setId_bitacora(bitacora.getId_bitacora());
				bitacora_banorte.setId_card(bitacora.getId_card());
				bitacora_banorte.setId_usuario(bitacora.getId_usuario());
				bitacora_banorte.setIdioma(bitacora.getIdioma());
				bitacora_banorte.setTransaction_number(0);
				bitacora_banorte.setBank_reference(bitacora.getBank_reference());
				bitacora_banorte.setComision(bitacora.getComision());
				bitacora_banorte.setTexto( Normalizer
				        .normalize(bitacora.getTexto(), Normalizer.Form.NFD)
				        .replaceAll("[^\\p{ASCII}]", ""));
				bitacora_banorte.setReferencia("");
				bitacora_banorte.setResultado_payw(bitacora.getResultado_payw()== null ? "" : bitacora.getResultado_payw());
				bitacora_banorte.setResultado_aut(bitacora.getResultado_aut() == null ? "" : bitacora.getResultado_aut());
				bitacora_banorte.setBanco_emisor("");
				bitacora_banorte.setTipo_tarjeta("");
				bitacora_banorte.setMarca_tarjeta("");
				bitacora_banorte.setCodigo_aut(bitacora.getCodigo_aut() == null ? "" : bitacora.getCodigo_aut());
				bitacora_banorte.setStatus3DS(bitacora.getStatus3DS() == null ? "" : bitacora.getStatus3DS() );
				bitacora_banorte.setConcepto(bitacora.getConcepto());
				bitacora_banorte.setTarjeta_transferencia(bitacora.getTarjeta_transferencia());
				bitacora_banorte.setNombre_destino(bitacora.getNombre_destino());
				bitacora_banorte.setCuenta_clabe_destino(bitacora.getCuenta_clabe_destino());
				bitacora_banorte.setBanco_destino(bitacora.getBanco_destino());
				bitacora_banorte.setId_aplicacion(bitacora.getId_aplicacion());
				bitacora_banorte.setLat(bitacora.getLat());
				bitacora_banorte.setLon(bitacora.getLon());
				//insertando en bitacora McBanorte
				mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			}
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL INSERTAR REGISTRO EN BITACORA BANORTE", ex);
		}
	}
	
	/*public BillPocketResponse sendAutorizationManual(Integer idApp, Integer idPais, String idioma, BPPaymentRequest data){
		BillPocketResponse response = null;
		AutorizationManualRequest authorization = new AutorizationManualRequest();
		try{
			Card card = mapper.getCard(data.getIdUser(), data.getIdCard(), idApp);
			
			authorization.setCargo(data.getAmount());
			authorization.setComision(data.getComision());
			authorization.setConcepto(data.getConcept());
			authorization.setCt(UtilsService.getSMS(card.getCt()));
			authorization.setEmisor("emisor");
			authorization.setExpDate(UtilsService.getSMS(card.getVigencia()));
			authorization.setIdEstablecimiento(0);
			authorization.setIdioma(idioma);
			authorization.setIdProducto(1);
			authorization.setIdProveedor(6);
			authorization.setIdUsuario(data.getIdUser());
			authorization.setImei(data.getImei());
			authorization.setLat(data.getLat());
			authorization.setLon(data.getLon());
			authorization.setModelo(data.getModel());
			authorization.setOperacion("pago manual");
			authorization.setPan(UtilsService.getSMS(card.getNumerotarjeta()));
			authorization.setReferencia(data.getReferencia());
			authorization.setSoftware(data.getSoftware());
			authorization.setTipoTarjeta(card.getIdfranquicia()+"");
			
			LOGGER.debug("Enviando peticion billpocket: " + GSON.toJson(authorization));
			response = billClient.AutorizationManual(authorization, idApp, idPais, idioma);
			LOGGER.debug("RESPUESTA PAGO MANUAL : " + GSON.toJson(response));
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR COBRO MANUAL COMERCIO", ex);
		}
		return response;
	}*/
	
	
	
	private ThreatMetrixResponse validatePayment(PaymentRequest pago, String sessionId, Integer idApp) {
		ThreatMetrixRequest request = new ThreatMetrixRequest();
		ThreatMetrixResponse validate = null;
		User usuarioVO = null;
		Card card = null;
		
		try {
			usuarioVO = mapper.getUserData(pago.getIdUser(), idApp); //mapper.getUsuario(Long.valueOf(pago.getIdUsuario()), pago.getIdTarjeta());
			card = mapper.getCard(pago.getIdUser(), pago.getIdCard(), idApp);
			LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getUsr_nombre()+", TARJETA: "+card.getNumerotarjeta()
			+", VIGENCIA: "+card.getVigencia()+", CVV: "+card.getCt()+",  ID TARJETA: "+pago.getIdCard());
			request.setApplicationName("mobilecardmx");
			request.setCantidad(pago.getAmount()+pago.getComision());
			request.setCardNumber(AddcelCrypto.decryptTarjeta(card.getNumerotarjeta()).substring(0, 6));
			request.setCorreo(usuarioVO.geteMail());
			request.setNombre(usuarioVO.getUsr_nombre());
			request.setSessionId(sessionId);
			request.setTelefono(usuarioVO.getUsr_telefono());
			request.setUsername(usuarioVO.getUsr_nombre());
			validate = tmClient.investigatePayment(request);
		} catch (Exception e) {
			e.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setCode(500);
			validate.setMessage("No se puedo realizar la validacion, intente de nuevo. ");
		}
		LOGGER.info("RESPONSE VALIDATE USER THREATMETRIX - {}", GSON.toJson(validate));
		return validate;
	}
	
	private String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			digest = bigIntDgst.toString(16);
			digest = String.format("%040x", bigIntDgst);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	public boolean isActive(Integer idApp){
		boolean active = true;
		String band = null;
		//if(idApp == 4){
		band = mapper.isAppActive(idApp);
		if(band != null)
			if(band.equals("T"))
				active = true;
			else
				active = false;
		//}
		if(active)
			LOGGER.debug("SERVICIO AUTORIZADO PARA ID {}", idApp);
		else
			LOGGER.debug("SERVICIO DENEGADO PARA ID {}", idApp);
		return active;
	}
	
}
