package com.addcel.mch2hpayment.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.mch2hpayment.spring.model.AmexAutorization;
import com.addcel.mch2hpayment.spring.model.RespuestaAmex;
import com.google.gson.Gson;

@Service
public class AmexService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmexService.class);
	
	private static final String AMEX_WEB_URL = "http://localhost:80/AmexWeb/AmexAuthorization";

	private  Gson gson = new Gson();
	
	
	/*
	 * respAmex.getCode() == "000" Aprovado
	 */
	public RespuestaAmex solicitaAutorizacionAmex(AmexAutorization amexAut) {
		String json = null;
		double total = 0.0;
		RespuestaAmex respAmex;
		try {
			total = amexAut.getMonto() + amexAut.getComision();
			
			LOGGER.info("SOLICITANDO AUTORIZACION AMEX - TARJETA: "+amexAut.getTarjeta()
					+",  VIGENCIA: "+amexAut.getVigencia().replace("/", "")+", MONTO: "+total
					+", CID: "+amexAut.getCvv2()+",  DIR: "+amexAut.getDom_amex());
			RestTemplate restClient = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			//datos en claro
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		    map.add("tarjeta", amexAut.getTarjeta());
		    map.add("vigencia", amexAut.getVigencia().replace("/", ""));
		    map.add("producto", amexAut.getProducto());
		    map.add("afiliacion", amexAut.getAfiliacion());
		    map.add("cid", amexAut.getCvv2());
		    map.add("monto", String.valueOf(total));
		    map.add("direccion", amexAut.getDom_amex());
		    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		    
		    
		    ResponseEntity<String> response  = restClient.postForEntity(AMEX_WEB_URL, request, String.class);
		    json = response.getBody();
			LOGGER.info("RESPUESTA DE AMEX: "+json);
			respAmex = gson.fromJson(json, RespuestaAmex.class);
			LOGGER.debug("RESPUESTA AMEX - CODE: " + respAmex.getCode() + ", TRAN: " + respAmex.getTransaction() 
					+ ", DESCRIPT:  " + respAmex.getDsc()+", ERROR: "+respAmex.getError() +", ERROR DESC: "+respAmex.getErrorDsc());

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error al enviar pago Amex...", e);
			respAmex = new RespuestaAmex();
			respAmex.setCode("101");
		}
		return respAmex;
	}
	
}
