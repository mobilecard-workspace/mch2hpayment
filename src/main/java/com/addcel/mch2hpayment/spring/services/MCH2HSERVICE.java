package com.addcel.mch2hpayment.spring.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueueAccountSignUpRequest;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryRequest;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.ServiceMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.FromAccount;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.ProcomVO;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;
import com.google.gson.Gson;

@Service
public class MCH2HSERVICE {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCH2HSERVICE.class);
	
	//@Autowired
	private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	private Gson gson = new Gson();
	@Autowired
	ServiceMapper mapper;
	
	public void updateTranfers(long id_usuario){
		LOGGER.debug("ACTUALIZANDO USER "+ 	id_usuario);
		mapper.updateuserSMS(id_usuario);
		LOGGER.debug("USUARIO ACTUALIZADO");
	}
	
	public LoginResponse login(){
		try{
			LoginRequest login  = new LoginRequest();
			login.setLogin("addcel_banorte");
			login.setPassword("B4N0rt3%");
			LOGGER.debug("iniciando login");
			LoginResponse response = h2hclient.login(login, "es");
			LOGGER.debug("LOGUIN: "+  response.getToken() + " " + response.getResult().isSuccess() + " " + response.getResult().getMessage());
			return response;
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			return new  LoginResponse();
		}
	}
	
	public LogoutResponse logout(){
		try{
			LogoutRequest logout = new LogoutRequest();
			logout.setLogin("addcel_banorte");
			logout.setToken(login.getToken());
			LogoutResponse response = h2hclient.logout(logout, "es");
			LOGGER.debug("[RESPUESTA DE LOGOUT]" +  response.getResult().isSuccess());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR EN LOGOUT]", ex);
			return new LogoutResponse();
		}
	}
	
	public MCSignUpResponse getToAccount(long idUsuario, String idioma){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			login = login();
			String responseBD = mapper.H2HBanorte_Business(idUsuario, 0, 0, "", 3,0,"" ,"",idioma);
			//LOGGER.debug("[RESPUESTA TO ACCOUNT] " + responseBD);
			response = gson.fromJson(responseBD, MCSignUpResponse.class);
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER CUANTAS ASOCIADAS ] ["+idUsuario+"]", ex);
		}finally{
			logout();
		}
		return response;
	}
	
	public BankCodesResponse getBankCodes(){
		BankCodesResponse response = new BankCodesResponse();
		try{
			LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS]");
			response.setBanks( mapper.getBankcodes());
			response.setIdError(0);
			response.setMensajeError("");
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
			response.setIdError(99);
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		}
		return response;
	}
	
	public MCSignUpResponse EnqueueAccountSignUp(MCAccount mcAccount){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			
			
			Calendar calendario = new GregorianCalendar();
			int hora =calendario.get(Calendar.HOUR_OF_DAY);
			
			
			//checar horario
			if( hora >= 22 || (hora >= 0 && hora <2)){
				LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				response.setAccounts(new ArrayList<FromAccount>());
				response.setIdError(88);
				if(mcAccount.getIdioma().equals("es"))
					response.setMensajeError("Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				else
					response.setMensajeError("Service available during the hours of 2:00 to 22:00.");
				
				return response;
			}
			
			
			int sizeNumber = mcAccount.getActNumber().length();
			
			int bin = mapper.isBinValido(mcAccount.getActNumber().substring(0,6));
			
			if(bin == 1 && sizeNumber != 10 && sizeNumber != 11 && sizeNumber != 18 ){
				response.setAccounts(new ArrayList<FromAccount>());
				response.setIdError(89);
				if(mcAccount.getIdioma().equals("es"))
					response.setMensajeError("El número de tarjeta no se acepta, favor de introducir número de cuenta o clabe");
				else
					response.setMensajeError("The card number not accepted, please enter account number or clabe");
				LOGGER.info("[DETECTAMOS BIN DE TARJETA] ");
				
				return response;
			}
			
			if(mcAccount.getBankCode().equals("072") || mcAccount.getBankCode().equals("032") )
			{
				
				if(sizeNumber > 11 )
				{
					response.setAccounts(new ArrayList<FromAccount>());
					response.setIdError(90);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Para banco Banorte, introduzca el número de cuenta");
					else
						response.setMensajeError("For Banorte bank, enter the account number");
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");
					
					return response;
				}else{
					if( mcAccount.getActType().equals("CLABE") && (sizeNumber == 10 || sizeNumber == 11))
						mcAccount.setActType("ACT");
				}
				
				
			}
			else{
				if(sizeNumber < 18){
					response.setAccounts(new ArrayList<FromAccount>());
					response.setIdError(91);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Para bancos distintos a Banorte, introduzca la clabe interbancaria");
					else
						response.setMensajeError("For banks other than Banorte, enter the interbank clabe");
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");
					return response;
				}else{
					if( mcAccount.getActType().equals("ACT") && sizeNumber == 18)
						mcAccount.setActType("CLABE");
				}
			}
			
			//verificar clave banco con cuenta
			if( mcAccount.getActType().equals("CLABE")){
				if(!mcAccount.getActNumber().substring(0, 3).equals(mcAccount.getBankCode())){
					response.setIdError(92);
					if(mcAccount.getIdioma().equals("es"))
						response.setMensajeError("Verifique que la cuenta introducida pertenezca al banco seleccionado.");
					else
						response.setMensajeError("Verify that the account entered belongs to the selected bank.");
					LOGGER.info("[DETECTAMOS CUENTA INCONSISTENTE CODIGOBANK vs NUMCUENTA] " );
					return response;
				}
			}
			
			login = login();
			EnqueueAccountSignUpRequest signup = new EnqueueAccountSignUpRequest();
			long clientReference = mapper.getMaxIdBitacora();
			
			if(mcAccount.getRfc().length() > 13)
				mcAccount.setRfc(mcAccount.getRfc().substring(0, 13));
			
			if(mcAccount.getActType().equals("ACT") && mcAccount.getActNumber().length() > 11)
				mcAccount.setActType("CLABE");
			
			
			AccountRecord account = new AccountRecord();
			account.setActNumber(mcAccount.getActNumber().trim());
			account.setActType(mcAccount.getActType().trim());
			account.setBankCode(mcAccount.getBankCode().trim());
			account.setContact(mcAccount.getContact()== null ? "" : Business.removeAcentos(mcAccount.getContact()).trim() );//opcional
			account.setEmail(mcAccount.getEmail().trim());
			account.setHolderName(Business.removeAcentos(mcAccount.getHolderName()).trim()); //obligatorio
			account.setPhone(mcAccount.getPhone()== null ? "" :mcAccount.getPhone().trim() ); //opcional
			account.setRfc(mcAccount.getRfc().trim());
			signup.setAccount(account);
			signup.setClientReference(new Date().getSeconds()+""+clientReference); //verificar al final
			signup.setIdAccount(""); // opcional Banorte genera uno
			signup.setToken(login.getToken());
			//LOGGER.info("Request alta cuenta " + gson.toJson(signup));
			EnqueueTransactionResponse responseBanorte = h2hclient.EnqueueAccountSignUp(signup, mcAccount.getIdioma());
			//LOGGER.debug("RESPUESTA ALTA " + responseBanorte.getResult().isSuccess() + " " + responseBanorte.getTransactionReference());
			if(responseBanorte.getResult().isSuccess()){
				String responsBD =  mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0, Long.parseLong(responseBanorte.getTransactionReference()), mcAccount.getAlias(), 2,0,"","", mcAccount.getIdioma());
				response = gson.fromJson(responsBD, MCSignUpResponse.class);
				//LOGGER.debug("[RESPONSE BD] " +  responsBD);
			}else
			{
				//String responsBD =  mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0, Long.parseLong(responseBanorte.getTransactionReference()), mcAccount.getAlias(), 2, mcAccount.getIdioma());
				//response = gson.fromJson(responsBD, MCSignUpResponse.class);
				//LOGGER.debug("[RESPONSE BD] " +  responsBD);
				response.setIdError(10);
				response.setMensajeError(responseBanorte.getResult().getMessage());
			}
			logout();
			return response;
		}
		catch(Exception ex){
			LOGGER.error("[ERROR AL REGISTAR CUENTA USUARIO]", ex);
			return response;
		}finally{
			//logout();
		}
	}
	
	/**
	 * Actualiza cuenta destino
	 * @param request
	 * @return
	 */
	public MCSignUpResponse updateAccount(UpdateAccountRequest request){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			LOGGER.debug("[INICIANDO ACTUALIZACION CUENTA ]" + request.getIdUsuario());
			String responseDB = mapper.H2HBanorte_Business(request.getIdUsuario(), 0, 0, request.getAlias().trim(), 4, request.getIdAccount(),request.getTelefono().trim(), request.getEmail().trim() ,request.getIdioma().trim()); // proceso 4
			//LOGGER.debug("RESPONSE JSON: " + responseDB);
			response = gson.fromJson(responseDB, MCSignUpResponse.class);
		}catch(Exception ex){
			response.setIdError(99);
			if(request.getIdioma().equalsIgnoreCase("es"))
				response.setMensajeError("Error inesperado, vuelva a intentarlo");
			else
				response.setMensajeError("Unexpected error, try again");
		//	LOGGER.error("[ERROR AL ACTUALIZAR CUENTA DESTINO ] " + gson.toJson(request), ex);
		}
		return response;
	}
	
	public MCSignUpResponse deleteAccount(long idUsuario, long idAccount, String idioma){
		MCSignUpResponse response = new MCSignUpResponse();
		try{
			LOGGER.debug("[INICIANDO ELIMINACION DE CUENTA ]" + idUsuario);
			String responseDB = mapper.H2HBanorte_Business(idUsuario, 0, 0, "", 5,idAccount,"","",idioma); // proceso 5
		//	LOGGER.debug("RESPONSE JSON: " + responseDB);
			response = gson.fromJson(responseDB, MCSignUpResponse.class);
		}catch(Exception ex){
			if(idioma.equalsIgnoreCase("es"))
				response.setMensajeError("Error inesperado, vuelva a intentarlo");
			else
				response.setMensajeError("Unexpected error, try again");
			//LOGGER.error("[ERROR AL ELIMINAR CUENTA DESTINO ] " + idUsuario + "-" + idAccount , ex);
		}
		
		return response;
	}
	
	
	/*
	 * Realiza la consulta del status de una transacción encolada en el H2H Banorte.
	 */
	public TransactionEnquiryResponse TransactionEnquiry(String transactionReference, String idioma ){
		try{
			login = login();
			TransactionEnquiryRequest request = new TransactionEnquiryRequest();
			request.setToken(login.getToken());
			request.setTransactionReference(transactionReference);
			LOGGER.debug("[CONSULTANDO ESTATUS DE TRANSACCION] " + request.getTransactionReference());
			TransactionEnquiryResponse  response = h2hclient.TransactionEnquiry(request,idioma);
			LOGGER.debug("[FINALIZANDO CONSULTA DE ESTATUS]" + request.getTransactionReference());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR AL CONSULTAR ESTATUS DE TRANSACCION] " + transactionReference, ex);
			return null;
		}finally{
			logout();
		}
	}
	
	/*
	 * Encola una transacción para transferencia
	 * Se compara RFC,NUM CUENTA, COD BANCO, TIPO CUENTA para determinar que estén registradas
	 */
	public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora){
		EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		MCTransafersResponse responseTransfer = new MCTransafersResponse();
		ModelAndView view = new ModelAndView("transfers_response");
		McBaseResponse McResponse = new McBaseResponse();
		try{
			login = login();
			TBitacoraVO bitacora = new TBitacoraVO();
			
			SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			responseTransfer.setComision(1);
			responseTransfer.setFecha(DFormat.format(new Date()));
			responseTransfer.setMontoTransfer(paymentRequest.getAmount());
			
			
			
			User user = mapper.getUserData(paymentRequest.getIdUser());
			Card card = mapper.getCard(paymentRequest.getIdUser(),paymentRequest.getIdCard());
			AccountRecord fromAccount = new AccountRecord();
			
			responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));
			
			fromAccount.setActNumber("0276948693"); //card.getNum_cuenta()
			fromAccount.setActType("ACT"); //card.getAct_type()
			fromAccount.setBankCode("072"); //card.getIdbanco()
			fromAccount.setContact("Jorge Silva Gallegos"); //card.getNombre_tarjeta()
			fromAccount.setEmail("jorge@mobilecardmx.com"); //user.geteMail()
			fromAccount.setHolderName("ADCEL SA DE CV"); //card.getNombre_tarjeta()
			fromAccount.setPhone("5555403124"); //user.getUsr_telefono()
			fromAccount.setRfc("ADC090715DW3");
			
			AccountRecord toAccount = mapper.getAccount(paymentRequest.getAccountId()); //new AccountRecord();
			
		//	LOGGER.debug("To Account: " + paymentRequest.getAccountId() + " " + gson.toJson(toAccount));
			//LOGGER.debug("From Account: " + fromAccount);
			
			/*bitacora.setIdUsuario(paymentRequest.getIdUser()+"");
			bitacora.setAfiliacion("afiliacion");
			bitacora.setBitCargo("bitCargo");
			bitacora.setBitCodigoError(0);
			bitacora.setConcepto("concepto");//.setBitConcepto("bitConcepto");
			bitacora.setBitFecha(new Date().toString());
			bitacora.setBitHora(new Date().toString());
			bitacora.setBitNoAutorizacion("");
			bitacora.setBitStatus(3);
			bitacora.setBitTicket("bitTicket");
			bitacora.setComision(0.0);
			
			bitacora.setDestino("destino");
			bitacora.setIdProveedor(1);
			bitacora.setIdProducto(1);
			bitacora.setTarjeta_compra(card.getNumerotarjeta());
			
			mapper.addBitacora(bitacora);*/
			
			payment.setAmount(paymentRequest.getAmount());
			payment.setClientReference(idBitacora+""); //"ACC000011"
			payment.setDescription(paymentRequest.getConcept());
			payment.setFromAccount(fromAccount);
			payment.setToAccount(toAccount);
			payment.setToken(login.getToken());
			response = h2hclient.EnqueuePayment(payment,paymentRequest.getIdioma());
			
		//	LOGGER.debug("[RESPUESTA PAGO] " + gson.toJson(response));
			
			if(response.getResult().isSuccess()){
				//String responseBD = mapper.H2HBanorte_Business(paymentRequest.getIdUser(), bitacora.getIdBitacora(), Long.parseLong(response.getTransactionReference()), "", 1, "es");
				LOGGER.debug("[Transaccion guardada] Exito");
				mapper.updateTbitacora(idBitacora,1,paymentRequest.getAuthProcom());
				
			}
			else
			{
				LOGGER.debug("[Transaccion guardada] error al cobrar");
				mapper.updateTbitacora(idBitacora,2,"");
			}
			
			/*SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			responseTransfer.setComision(1);
			responseTransfer.setFecha(DFormat.format(new Date()));
			responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			responseTransfer.setMontoTransfer(paymentRequest.getAmount());
			responseTransfer.setReferenceBanorte(response.getTransactionReference());
			responseTransfer.setTarjeta(fromAccount.getActNumber());*/
			responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			responseTransfer.setReferenceBanorte(response.getTransactionReference());
			responseTransfer.setRefProcom(paymentRequest.getRefProcom());
			
			McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
			McResponse.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
			//LOGGER.debug("JSON: " +  gson.toJson(responseTransfer));
			view.addObject("json", gson.toJson(responseTransfer));
			return view;//McResponse;
		}catch(Exception ex){
			LOGGER.error("[ERROR AL ENCOLAR PAGO] " + payment.getClientReference(), ex);
			responseTransfer.setIdError(99);
			responseTransfer.setMensajeError("Error al procesar Transaferencia");
			view.addObject("json", gson.toJson(responseTransfer));
			return view;//McResponse;
		}finally{
			logout();
		}
	}
	
	public ModelAndView ProsaResponse(String EM_Response,String EM_Total, String EM_OrderID,String EM_Merchant, String EM_Store,
			String EM_Term, String EM_RefNum,String EM_Auth, String EM_Digest, String cc_numberback){
		
		ModelAndView view = new ModelAndView("transfers_response");
		long id_bitacora =  Long.parseLong(EM_OrderID);
		TBitacoraBanorte bitacoraBanorte = mapper.getBitacoraBanorte(id_bitacora);
		PaymentRequest paymentRequest = new PaymentRequest();
		paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
		paymentRequest.setAmount(bitacoraBanorte.getAmount());
		paymentRequest.setConcept("Transferencia"); //TODO
		paymentRequest.setIdCard(bitacoraBanorte.getId_card());
		paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
		paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
		paymentRequest.setAuthProcom(EM_Auth);
		paymentRequest.setRefProcom(EM_RefNum);
		if(EM_Response.equals("approved")) //EM_Response.equals("approved")
		{
			view = EnqueuePayment(paymentRequest,id_bitacora);
			mapper.updateBitacoraBanorte(100,id_bitacora, EM_Auth);
			//LOGGER.debug("Json Respuesta Mobilecard : " + view.getModel().get("json"));
		}
		else{
			McBaseResponse McResponse = new McBaseResponse();
			McResponse.setIdError(90);
			McResponse.setMensajeError("Error al procesar cobro 3ds");
			MCTransafersResponse responseTransfer = new MCTransafersResponse();
			responseTransfer.setIdError(99);
			responseTransfer.setMensajeError("Error al procesar Transaferencia");
			view.addObject("json", gson.toJson(McResponse));
		}
		return view;
		
	}
	
	public ModelAndView send3ds(PaymentRequest paymentRequest){
		
		return comercioFin(paymentRequest);
	}
	
	public ModelAndView comercioFin(PaymentRequest paymentRequest){
		ModelAndView mav = new ModelAndView("comerciofin");
		try{
			
			TBitacoraVO bitacora = new TBitacoraVO();
			Card card = mapper.getCard(paymentRequest.getIdUser(), paymentRequest.getIdCard()); //
			String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			String vig = UtilsService.getSMS(card.getVigencia());
			String ct = UtilsService.getSMS(card.getCt()); 
			String mes = vig.split("/")[0];
			String anio = "20"+vig.split("/")[1];
			double comision = 0.0; //TODO
			
			bitacora.setTipo("");
			bitacora.setSoftware("");
			bitacora.setModelo("");
			bitacora.setWkey("");
			bitacora.setIdUsuario(paymentRequest.getIdUser()+"");
			bitacora.setIdProducto(1);
			bitacora.setCar_id(1);
			bitacora.setCargo(paymentRequest.getAmount()+comision);
			bitacora.setIdProveedor(1);
			bitacora.setConcepto("Transferencia H2HBANORTE: "  + " Monto: "
					+ paymentRequest.getAmount() + " Comision: " + comision );
			bitacora.setImei("");
			bitacora.setDestino("Transferencia Banorte");
			bitacora.setTarjeta_compra(numcard);
			bitacora.setAfiliacion(Constantes.MERCHANT);
			bitacora.setComision(comision);
			bitacora.setCx("");
			bitacora.setCy("");
			bitacora.setPin("");
			mapper.addBitacora(bitacora);
			
			TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			bitacora_banorte.setAccount_id(paymentRequest.getAccountId());
			bitacora_banorte.setAmount(paymentRequest.getAmount());
			bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			bitacora_banorte.setId_card(paymentRequest.getIdCard());
			bitacora_banorte.setId_usuario(paymentRequest.getIdUser());
			bitacora_banorte.setIdioma(paymentRequest.getIdioma());
			bitacora_banorte.setTransaction_number(0);
			bitacora_banorte.setBank_reference("");
			
			//insertando en bitacora McBanorte
			mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			
			LOGGER.debug("ID BITACORA: " + bitacora.getIdBitacora());
			double total = comision + paymentRequest.getAmount();
			String varTotal =  Business.formatoMontoProsa(total+"");
			String digest = Business.digest(
					new StringBuilder(Constantes.MERCHANT).append(Constantes.STORE).append(Constantes.TERM)
							.append(varTotal).append(Constantes.CURRENCY).append(bitacora.getIdBitacora()).toString());
			
			ProcomVO procom = new ProcomVO(varTotal, Constantes.CURRENCY, Constantes.ADDRESS, bitacora.getIdBitacora()+"", 
						Constantes.MERCHANT, Constantes.STORE, Constantes.TERM, digest,
						"http://199.231.160.203:8089/MCH2HPayment/comercio-con",card.getNombre_tarjeta(), 
						numcard,card.getIdfranquicia() == 1 ? "Visa" : "Mastercard",mes,anio, "", ct);
			mav.addObject("prosa",procom);
		}catch(Exception ex){
			LOGGER.error("[ERRO AL PROCESAR PAGO]", ex);
			McBaseResponse McResponse = new McBaseResponse();
			McResponse.setIdError(90);
			McResponse.setMensajeError("Error al procesar cobro 3ds");
			MCTransafersResponse transfersresponse = new MCTransafersResponse();
			transfersresponse.setIdError(98);
			transfersresponse.setMensajeError("Error al procesar Transferencia");
			
			mav =  new ModelAndView("transfers_response");
			mav.addObject("json", gson.toJson(transfersresponse));
			//mav.addObject("json", gson.toJson(McResponse));
		}
		return mav;
	}
	
	
}
