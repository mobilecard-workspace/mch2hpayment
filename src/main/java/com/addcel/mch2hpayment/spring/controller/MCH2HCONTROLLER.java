package com.addcel.mch2hpayment.spring.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.H2HCLIENT;
import com.addcel.mch2hpayment.spring.services.MCH2HSERVICE;
import com.addcel.mch2hpayment.utils.Constantes;



@RestController
public class MCH2HCONTROLLER {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCH2HCONTROLLER.class);
	
	@Autowired
	MCH2HSERVICE mcH2HService;
	
	
	@RequestMapping(value = "/pagina-error",method = RequestMethod.GET)
	public ModelAndView pagina_error(@RequestParam("idApp") int idApp){
		ModelAndView view = new ModelAndView("payworks/pagina-error");
		view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
		view.addObject("idApp", idApp);
		return view;
	}
	
	@RequestMapping(value = "/payw2",method = RequestMethod.GET)
	public ModelAndView payw2(@RequestParam("idApp") int idApp){
		ModelAndView view = new ModelAndView("payworks/comercioPAYW2");
		view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
		view.addObject("idApp", idApp);
		return view;
	}

	@RequestMapping(value = "/exito",method = RequestMethod.GET)
	public ModelAndView exito(@RequestParam("idApp") int idApp){
		ModelAndView view = new ModelAndView("payworks/exito");
		SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");;
		view.addObject("mensajeError", "<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/>" 
							
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+DFormat.format(new Date())+"</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+"111111111" + "</td></tr>"
								
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+"0000123" + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "+"123456" + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+"123.00" + "</td></tr>"
								+"<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"+"34.00" + "</td></tr>"
								+ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>"); // Favor de guardar este comprobante de pago para "
				//+ "posibles aclaraciones.");
		
		
		view.addObject("idApp", idApp);
		return view;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/send3ds",method = RequestMethod.GET)
	public ModelAndView ejemplosend(){
		ModelAndView view = new ModelAndView("ejemplosend");
		
		return view;
	}
	
	@RequestMapping(value = "/updateSMS",method = RequestMethod.GET)
	public ModelAndView actualizauser(@RequestParam long idUser){
		ModelAndView view = new ModelAndView("ejemplosend");
		mcH2HService.updateTranfers(idUser);
		return view;
	}
	
	@RequestMapping(value = "/login",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String login(){
		mcH2HService.login();
		return "login";
	}
	
	@RequestMapping(value = "/logout",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String logout(){
		mcH2HService.logout();
		return "login";
	}
	
	/**
	 * Obtener las cuentas asociadas a un usuario
	 * @param idUsuario
	 * @param idioma
	 * @return
	 */
	@RequestMapping(value = "/toAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse getToAccount(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma){
		
		return mcH2HService.getToAccount(idUsuario,idioma);
	}
	
	/*
	 * Encola una transacción de alta de registro de cuenta en la plataforma H2H banorte.
	 */
	@RequestMapping(value = "/enqueueAccountSignUp",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse EnqueueAccountSignUp(@RequestBody MCAccount mcAccount){
		
		return mcH2HService.EnqueueAccountSignUp(mcAccount);
	}
	
	/*
	 * Realiza la consulta del status de una transacción encolada en el H2H Banorte.
	 */
	@RequestMapping(value = "/transactionEnquiry",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public TransactionEnquiryResponse TransactionEnquiry(){
		String transactionReference = "7";
		String idioma = "es";
		return mcH2HService.TransactionEnquiry(transactionReference, idioma);
	}
	
	/**
	 * Encolar una transferencia en Banorte
	 * @param payment
	 * @return
	 */
	@RequestMapping(value = "/enqueuePayment",method = RequestMethod.POST) //, produces = "application/json;charset=UTF-8"
	public ModelAndView EnqueuePayment(@RequestParam String concept, @RequestParam long idUser,
			@RequestParam long idCard, @RequestParam long accountId, @RequestParam double amount, 
			@RequestParam String idioma ){ //McBaseResponse @RequestBody 
		
		PaymentRequest payment = new PaymentRequest();
		payment.setAccountId(accountId);
		payment.setAmount(amount);
		payment.setConcept(concept);
		payment.setIdCard(idCard);
		payment.setIdioma(idioma);
		payment.setIdUser(idUser);
		return mcH2HService.send3ds(payment);
		//return mcH2HService.EnqueuePayment(payment);
	}
	
	
	//EnqueueTransactionResponse
	
	
	
	@RequestMapping(value = "/comerciofin",method = RequestMethod.GET)
	public ModelAndView comerciofin(){
		/*ModelAndView mav = new ModelAndView("comerciofin");
		TBitacoraVO bitacora = new TBitacoraVO();
		
		bitacora.setTipo("");
		bitacora.setSoftware("");
		bitacora.setModelo("");
		bitacora.setWkey("");
		bitacora.setIdUsuario("123456");
		bitacora.setIdProducto(1);
		bitacora.setCar_id(1);
		bitacora.setCargo(1);
		bitacora.setIdProveedor(1);
		bitacora.setConcepto("Transferencia H2HBANORTE: " + "compra.getTarjeta()" + " "
				+ "producto.getMonto()" + " Comision: " + "compra.getComision()" );
		bitacora.setImei("compra.getImei()");
		bitacora.setDestino("compra.getTarjeta()");
		bitacora.setTarjeta_compra("usuario.getNumTarjeta()");
		bitacora.setAfiliacion(Constantes.MERCHANT);
		bitacora.setComision(2);
		bitacora.setCx("compra.getCx()");
		bitacora.setCy("compra.getCy()");
		bitacora.setPin("compra.getPin()");
		
		return mav;*/
		
		return mcH2HService.comercioFin(new PaymentRequest());//comercioFin(8923233103984l, 447);
	}
	
	
	/**
	 * REspuesta de prosa
	 * @param EM_Response
	 * @param EM_Total
	 * @param EM_OrderID
	 * @param EM_Merchant
	 * @param EM_Store
	 * @param EM_Term
	 * @param EM_RefNum
	 * @param EM_Auth
	 * @param EM_Digest
	 * @param cc_numberback
	 * @return
	 */
	@RequestMapping(value = "/comercio-con", method = RequestMethod.POST) //McBaseResponse
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest, @RequestParam String cc_numberback) {
		
		
		LOGGER.debug(EM_Response);
		LOGGER.debug(EM_Total);
		LOGGER.debug(EM_OrderID);
		LOGGER.debug(EM_Merchant);
		LOGGER.debug(EM_Store);
		LOGGER.debug(EM_Term);
		LOGGER.debug(EM_RefNum);
		LOGGER.debug(EM_Auth);
		LOGGER.debug(EM_Digest);
		LOGGER.debug(cc_numberback);
		
		return mcH2HService.ProsaResponse(EM_Response, EM_Total, EM_OrderID, EM_Merchant, EM_Store, EM_Term, EM_RefNum, EM_Auth, EM_Digest, cc_numberback);
		/*ModelAndView mav= null;
		mav = new ModelAndView("responseprosa");
		mav.addObject("EM_Response", EM_Response);
		mav.addObject("EM_RefNum", EM_RefNum);				
		mav.addObject("EM_Auth", EM_Auth);
		
		McBaseResponse res = new McBaseResponse();
		res.setIdError(0);
		res.setMensajeError(EM_Response);
		
		return res;*/
	}
	
	/**
	 * Obtener todas las transacciones realizadas en Banorte H2H 
	 * @param idUsuario
	 * @param idioma
	 * @return
	 */
	@RequestMapping(value = "/transactions",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse getTransactions(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma){
		
		return mcH2HService.getToAccount(idUsuario,idioma);
	}
	
	@RequestMapping(value = "/bankCodes",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BankCodesResponse getBankCodes(){
		
		return mcH2HService.getBankCodes();
	}
	
	//actualizar cuenta asociada
	@RequestMapping(value = "/updateAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse updateAccount(@RequestBody UpdateAccountRequest request){
		
		return mcH2HService.updateAccount(request);
	}
	
	//eliminar cuenta asociada
	@RequestMapping(value = "/deleteAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse deleteAccount(@RequestParam("idUsuario") long idUsuario,@RequestParam("idAccount") long idAccount, @RequestParam("idioma") String idioma){
		
		return mcH2HService.deleteAccount(idUsuario,idAccount, idioma);
	}
	
	/*@RequestMapping(value = "/{app}/{idioma}/checkBalance", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BalanceResponse getBalance(@PathVariable String app,@PathVariable String idioma, @RequestParam("tag_number") String tag_number, @RequestParam("check_digit") String check_digit){
		//requestPay.getRestemplate();
		
		return telepeajeService.getBalance(idioma, tag_number, check_digit);
	}*/
	
}
