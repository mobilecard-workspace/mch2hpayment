package com.addcel.mch2hpayment.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.H2HService;
import com.addcel.mch2hpayment.spring.services.PaymentService;

@RestController
public class H2HController {

	private static final Logger LOGGER = LoggerFactory.getLogger(H2HController.class);
	
	@Autowired 
	private H2HService H2HSer;
	
	@Autowired
	private PaymentService service;
	
	/**
	 * Obtener las cuentas asociadas a un usuario
	 * @param idUsuario
	 * @param idioma
	 * @return
	 */
	@RequestMapping(value = "{idApp}/toAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse getToAccount(@PathVariable("idApp") int idApp,  @RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma){
		if(service.isActive(idApp)){
			return H2HSer.getToAccount(idUsuario,idioma, idApp);
		}else{
			MCSignUpResponse error = new MCSignUpResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return error;
		}
	}
	
	/*
	 * Encola una transacción de alta de registro de cuenta en la plataforma H2H banorte.
	 */
	@RequestMapping(value = "{idApp}/enqueueAccountSignUp",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse EnqueueAccountSignUp(@PathVariable("idApp") int idApp,@RequestBody MCAccount mcAccount){
		if(service.isActive(idApp)){
		  return H2HSer.EnqueueAccountSignUp(mcAccount,idApp);
		}else{
			MCSignUpResponse error = new MCSignUpResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return error;
		}
	}
	
	/*
	 * Realiza la consulta del status de una transacción encolada en el H2H Banorte.
	 */
	@RequestMapping(value = "{idApp}/transactionEnquiry",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public TransactionEnquiryResponse TransactionEnquiry(@PathVariable("idApp") int idApp){
		String transactionReference = "7";
		String idioma = "es";
		return H2HSer.TransactionEnquiry(transactionReference, idioma, idApp);
	}
	
	
	@RequestMapping(value = "{idApp}/bankCodes",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BankCodesResponse getBankCodes(@PathVariable("idApp") int idApp){
		if(service.isActive(idApp)){
			return H2HSer.getBankCodes(idApp);
		}else{
			BankCodesResponse  error = new BankCodesResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return error;
		}
	}
	
	//actualizar cuenta asociada
	@RequestMapping(value = "{idApp}/updateAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse updateAccount(@PathVariable("idApp") int idApp, @RequestBody UpdateAccountRequest request){
		if(service.isActive(idApp)){	
			return H2HSer.updateAccount(request,idApp);
		}else{
			MCSignUpResponse error = new MCSignUpResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return error;
		}
	}
	
	//eliminar cuenta asociada
	@RequestMapping(value = "{idApp}/deleteAccount",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public MCSignUpResponse deleteAccount(@PathVariable("idApp") int idApp,@RequestParam("idUsuario") long idUsuario,@RequestParam("idAccount") long idAccount, @RequestParam("idioma") String idioma){
		if(service.isActive(idApp)){	
			return H2HSer.deleteAccount(idUsuario,idAccount, idioma, idApp);
		}else{
			MCSignUpResponse error = new MCSignUpResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return error;
		}
	}
	
	
}
