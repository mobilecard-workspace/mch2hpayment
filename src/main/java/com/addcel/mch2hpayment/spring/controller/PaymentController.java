package com.addcel.mch2hpayment.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.mch2hpayment.client.model.BillPocketResponse;
import com.addcel.mch2hpayment.spring.model.BPPaymentRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.Token;
import com.addcel.mch2hpayment.spring.services.PaymentService;

@RestController
public class PaymentController {
	
	private static final String PATH_GET_TOKEN = "/{idApp}/{idioma}/getToken";
	
	private static final String PROCESA_PAGO = "{idApp}/{idPais}/{idioma}/{accountId}/pagoBP";
	
	private static final String HEADER_TOKEN = "Authorization";
	
	private static final String HEADER_PROFILE = "Profile";
	
	@Autowired
	private PaymentService service;
	
	@RequestMapping(value = PATH_GET_TOKEN, method=RequestMethod.POST, headers="Content-Type=application/json")
	public ResponseEntity<Token> getToken(@PathVariable Integer idApp, @PathVariable String idioma,
			HttpServletRequest req) {
		try {
			if(req.getHeader(HEADER_TOKEN).isEmpty() && req.getHeader(HEADER_PROFILE).isEmpty()) {
				return new ResponseEntity<Token>((Token) new McBaseResponse(-1, "No estas autorizado para realizar esta operacion."), HttpStatus.UNAUTHORIZED);
			} else {
				return new ResponseEntity<Token>(service.getToken(req.getHeader(HEADER_TOKEN), req.getHeader(HEADER_PROFILE), 
						req.getSession().getId(), req.getRemoteAddr(),idApp), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Token>((Token) new McBaseResponse(-1, "Error interno, favor de contactar a soporte@addcel.com"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = PROCESA_PAGO, method=RequestMethod.POST)
	public ResponseEntity<Object> paymentBillPocket(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma,
			@PathVariable String accountId, @RequestBody BPPaymentRequest data, HttpServletRequest req) {
		ResponseEntity<Object> response = null;
		BPPaymentResponse authorization = null;
		try {
			
			if(service.validaToken(req.getHeader(HEADER_TOKEN), req.getRemoteAddr(), req.getSession().getId(), data, accountId,idApp,idPais,idioma) && service.isActive(idApp)) {
				authorization = service.processPayment(idApp, idPais, idioma, data);
				response = new ResponseEntity<>(authorization, HttpStatus.OK);
			} else {
				McBaseResponse error = new McBaseResponse();
	            error.setIdError(-10);
	            error.setMensajeError("No esta autorizado para realizar esta operacion.");
	            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			McBaseResponse error = new McBaseResponse();
            error.setIdError(-10);
            error.setMensajeError("ERROR: "+ e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            e.printStackTrace();
		}
		return response;
	}
	
}
