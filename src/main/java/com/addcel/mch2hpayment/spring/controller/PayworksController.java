package com.addcel.mch2hpayment.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.services.PayworksService;

@RestController
public class PayworksController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksController.class);
	
	@Autowired
	PayworksService payService;
	
	
	
	
	/**
	 * Encolar una transferencia en Banorte
	 * @param payment
	 * @return
	 */
	/*@RequestMapping(value = "payworks/enqueuePayment",method = RequestMethod.POST) //, produces = "application/json;charset=UTF-8"
	public ModelAndView EnqueuePayment(@RequestParam String concept, @RequestParam long idUser,
			@RequestParam long idCard, @RequestParam long accountId, @RequestParam double amount, @RequestParam(required=false) double comision,
			@RequestParam String idioma ){ //McBaseResponse @RequestBody 
		
		PaymentRequest payment = new PaymentRequest();
		payment.setAccountId(accountId);
		payment.setAmount(amount);
		payment.setConcept(concept);
		payment.setIdCard(idCard);
		payment.setIdioma(idioma);
		payment.setIdUser(idUser);
		payment.setComision(comision);
		return payService.send3DSBanorte(payment);
		//return mcH2HService.EnqueuePayment(payment);
	}*/
	
	/*@RequestMapping(value = "payworks/payworksRec3DRespuesta")
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		//return parworksService.procesaRespuesta3DPayworks(cadena, modelo);
		LOGGER.info("Respuesta 3DS Banorte");
		LOGGER.debug("Resposnse 3ds Banorte: "+cadena);
		return payService.payworksRec3DRespuesta(cadena, modelo);
	}*/
	
	
	/*@RequestMapping(value = "payworks/payworks2RecRespuesta", method = RequestMethod.GET)
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return payService.payworks2RecRespuesta(NUMERO_CONTROL, REFERENCIA, FECHA_RSP_CTE, TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);
	}
	
	@RequestMapping(value="transferResponse",method = RequestMethod.POST)
	public ModelAndView transferResponse(@RequestParam String json){
		ModelAndView view = new ModelAndView("transfers_response_pay");
		view.addObject("json", json);
		return view;
	}*/
	
	
}
