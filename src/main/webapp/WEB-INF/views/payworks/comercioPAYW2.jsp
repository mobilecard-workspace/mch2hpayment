<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<title>Redirección Payworks</title>
<c:choose>
	<c:when test="${idApp == 1}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#f57c00;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #f57c00;
    height: 55px;
    -webkit-appearance: none;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
	<c:when test="${idApp == 2}">
		<style type="text/css">
 * {
/*with these codes padding and border does not increase it's width.Gives intuitive style.*/
  -webkit-box-sizing: border-box;   
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#33313;
  font-family: "Arial", Helvetica, sans-serif;
}

body {
   margin:0;
   padding:0;		
   color: #616161;  
   background-color:#e0e0e0;
}
div#envelope{
	width:90%;
	margin: 16px auto 0 auto;
	background-color:#ffffff;
	padding:10px 0;
	/** border:1px solid gray; **/
	border-radius:10px;
} 
form{
	width:80%;
	margin:0 10%;
}  
form header {
  text-align:center;
  font-family: 'Roboto Slab', serif;
}
/* Makes responsive fields.Sets size and field alignment.*/
input[type=text],input[type=password]{
	margin-bottom: 20px;
	margin-top: 10px;
	width:100%;
	padding: 15px;
	border-radius:5px;
	border:1px solid #dbd9d6;
	font-size: 110%;
}
input[type=submit]
{
	margin-bottom: 20px;
	width:100%;
	color:#ffffff;
	padding: 15px;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
	height: 55px;
}
input[type=button]
{
	margin-bottom: 20px;
	width:100%;
	padding: 15px;
	color:#ffffff;
	border-radius:5px;
	background-color:#388e3c;
	font-weight: bold;
	font-size: 150%;
}
textarea{
	width:100%;
	padding: 15px;
	margin-top: 10px;
	border:1px solid #7ac9b7;
	border-radius:5px; 
	margin-bottom: 20px;
	resize:none;
}
input[type=text]:focus, textarea:focus, select:focus {
  border-color: #333132;
}
.styled-select {
	width:100%;
	overflow: hidden;
	background: #FFFFFF;
	border-radius:5px;
	border:1px solid #dbd9d6;
}
.styled-select select {
	font-size: 110%;
	width: 100%;
	border: 0 !important;
	padding: 15px 0px 15px 0px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	text-indent:  0.01px;
	text-overflow: '';
}
.btn-style {
    font-family: arial;
    font-weight: bold;
    width: 100%;
    /** opacity: 0.5; **/
    border: solid 1px #e6e6e6;
    border-radius: 3px;
    moz-border-radius: 3px;
    font-size: 16px;
    padding: 1px 17px;
    background-color: #388e3c;
    height: 55px;
}
table{
	width:80%;
	margin:0 10%;
}  
table header {
	text-align:center;
	font-family: 'Roboto Slab', serif;
}
		</style>
	</c:when>
</c:choose>
<!-- 
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>
-->
<script type="text/javascript">
	function sendform() {
		if (self.name.length === 0) {
			self.name = "gotoProsa";
		}
		//var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
		//document.form1.return_target.value = self.name.toString();
		//document.form1.target = "wnd";
		document.form1.submit();
	}
</script>

</head>
<body onload="sendform();">
	<form method="post" name="form1" action="https://via.pagosbanorte.com/payw2"><!--https://via.banorte.com/payw2  -->
		<input type="hidden" name="ID_AFILIACION" value="${payworks.ID_AFILIACION}" /> 
		<input type="hidden" name="USUARIO" value="${payworks.USUARIO}" /> 
		<input type="hidden" name="CLAVE_USR" value="${payworks.CLAVE_USR}" /> 
		<input type="hidden" name="CMD_TRANS" value="${payworks.CMD_TRANS}" /> 
		<input type="hidden" name="ID_TERMINAL" value="${payworks.ID_TERMINAL}" /> 
		<input type="hidden" name="MONTO" value="${payworks.Total}" /> 
		<input type="hidden" name="MODO" value="${payworks.MODO}" /> 
		<input type="hidden" name="REFERENCIA" value="${payworks.AUTORIZACION_H2H}" /> 
		<input type="hidden" name="NUMERO_CONTROL" value="${payworks.Reference3D}" /> 
		<input type="hidden" name="NUMERO_TARJETA" value="${payworks.Number}" /> 
		<input type="hidden" name="FECHA_EXP" value="${payworks.FECHA_EXP}" /> 
		<input type="hidden" name="CODIGO_SEGURIDAD" value="${payworks.CODIGO_SEGURIDAD}" /> 
		<%-- <input type="hidden" name="CODIGO_AUT" value="${prosa.auth}" /> --%> 
		<input type="hidden" name="MODO_ENTRADA" value="${payworks.MODO_ENTRADA}" />
		<%-- <input type="hidden" name="LOTE" value="${prosa.lote}" /> --%>
		<input type="hidden" name="URL_RESPUESTA" value="${payworks.URL_RESPUESTA}" />
		<input type="hidden" name="IDIOMA_RESPUESTA" value="${payworks.IDIOMA_RESPUESTA}" />
		<%
		    String xid = request.getParameter("XID");
			String cavv = request.getParameter("CAVV");
		    if (xid != null || xid != "" && cavv != null || cavv != "") {
		%>
		    <input type="hidden" name="XID" value="${payworks.XID}" />
			<input type="hidden" name="CAVV" value="${payworks.CAVV}" />
		<%
		    }
		%>
		
		
		<input type="hidden" name="ESTATUS_3D" value="${payworks.Status}" />
		<input type="hidden" name="ECI" value="${payworks.ECI}" />
	</form>
	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr class="cart_menu">
				<th class="description"><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="cart_description">
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</body>
</html>